﻿angular.module("TimeTrack", ['ngRoute', 'ui.bootstrap'])
    .run([
        '$rootScope',
        '$timeout',
        function ( $rootScope, $timeout ) {

            window.useRootScope = function ( fn ) {
                $timeout(function () {
                    fn($rootScope);
                });
            };

            window.debug = function () {
                $timeout(function () {
                    $rootScope.debugging = true;
                    $rootScope.$broadcast('debugging');
                });
            };

        }
    ])
    .config([
        '$routeProvider',
        '$locationProvider',
        function ( $routeProvider, $locationProvider ) {
            $routeProvider
                .when('/login', {
                    templateUrl: 'templates/login.htm',
                    controller: 'LoginController'
                })
                .when('/edit-timesheet', {
                    templateUrl: 'templates/edit-timesheet.htm',
                    controller: 'EditTimesheetController'
                })
                .when('/view-timesheet', {
                    templateUrl: 'templates/view-timesheet.htm',
                    controller: 'ViewTimesheetController'
                })
                .when('/manager-panel', {
                    templateUrl: 'templates/manager-panel.htm',
                    controller: 'ManagerPanelController'
                })
                .when('/logging', {
                    templateUrl: 'templates/logging.htm',
                    controller: 'LoggingController'
                })
                .when('/options', {
                    templateUrl: 'templates/options.htm',
                    controller: 'OptionsController'
                })
                .when('/choose-view', {
                    templateUrl: 'templates/choose-view.htm',
                    controller: function ( $scope, UserProfileService ) {

                        $scope.choose = {
                            selected: null,
                            select: function ( id ) {
                                $scope.choose.selected = id;
                            },
                            save: function ( ) {
                                UserProfileService.setHomeView($scope.choose.selected);
                            }
                        }

                    },
                    resolve: {
                        "UserProfileService": "UserProfileService"
                    }
                })
                .otherwise({
                    redirectTo: '/login'
                });

        }
    ])
    .directive('errSrc', function() {
        return {
            link: function(scope, element, attrs) {
                element.bind('error', function() {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    });