angular.module("TimeTrack")
    .directive("jsonEditor", function () {
        return {
            restrict: 'EA',
            scope: {
                model: '='
            },
            template: '<input type="text" ng-model="raw" class="form-control jsoneditor-input" /><button class="btn btn-primary jsoneditor-set" ng-click="set()">Set</button>',
            controller: function ( $scope ) {
                var editLock = false;

                $scope.$watch('model', function ( n ) {
                    if ( editLock ) {
                        editLock = false;
                        return;
                    }
                }, true);
                $scope.raw = angular.toJson($scope.model);

                $scope.set = function () {
                    var obj;
                    try {
                        obj = JSON.parse($scope.raw);
                    } catch ( e ) {
                        //  set invalid
                        console.log("invalid!", e);
                        return;
                    }
                    console.log("object", obj);
                    editLock = true;
                    $scope.model = obj;
                };
            }
        };
    });

