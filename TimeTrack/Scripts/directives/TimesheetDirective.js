angular.module("TimeTrack")
    .directive("timesheet", function () {

        return {
            restrict: "EAC",
            scope: {
                options: "=",          //  true
                entries: "=",          //  []
                modified: "="          //  []
            },
            templateUrl: "/templates/timesheet.htm",
            controller: "TimesheetController",
            link: function ( scope, el, attr ) {
                console.log("Link");
            }
        };
    })
    .controller("TimesheetController", [
        '$scope',
        'PayCodes',
        'Data',
        function ( $scope, PayCodes, Data ) {
            $scope.$on('debugging', function () { $scope.debugging = true; });

            $scope.$watch('options', function ( n ) {
                console.log("options", n);
                $scope.view.activeDay = n.activeDay || "";
                $scope.view.canEdit = !!n.canEdit;
                $scope.view.loading = !!n.loading;
                $scope.laborCodes.buildFromPayCodes(n.allowedPayCodes || []);
            }, true);

            $scope.$on('data-update', function ( n ) {
                $scope.autocomplete.jobs = Data.Jobs;
                $scope.autocomplete.service = Data.Service;
                $scope.autocomplete.rental = Data.Rental;
            }, true);

            $scope.$watch('entries', function ( n ) {
                $scope.modified = [];   //clear out modifications since all the entries are now fresh
                $scope.liveEntries = JSON.parse(JSON.stringify(n)); // breaks scope binding

                console.log("new entries from parent!", n);
            }, true);

            function hashKeySort (a, b) {
                if ( a.hashKey > b.hashKey ) {
                    return 1;
                }
                if ( a.hashKey < b.hashKey) {
                    return -1;
                }
                return 0;
            }
            $scope.$watch('liveEntries', function ( n ) {
                //  iterate over
                console.time("Calculate Modified Entries");
                $scope.modified = [];
                var o = $scope.entries;
                n.sort(hashKeySort);
                o.sort(hashKeySort);

                var originalKeys = [];
                for ( var x = 0; x < o.length; x++ ) {
                    if ( originalKeys.indexOf(o[x].hashKey) == -1 ) {
                        originalKeys.push(o[x].hashKey);
                    }
                    if ( n[x].hashKey != o[x].hashKey ) {
                        console.log("MisMatch!! ", n[x], o[x]);
                        continue;
                    }
                    if ( n[x].Frozen ) {
                        //  just compare the hours since other stuff can't change
                        if ( n[x][$scope.options.activeDay] != o[x][$scope.options.activeDay]) {
                            $scope.modified.push(n[x]);
                        }
                        continue;
                    }
                    $scope.modified.push(n[x]);
                }

                console.log("Changed!", $scope.modified);
                console.timeEnd("Calculate Modified Entries");
            }, true);


            //  Stores which features are enabled in the current timesheet object
            $scope.remove = function ( index ) {
                $scope.entries.splice(index, 1);
            };

            $scope.laborCodes = {
                Job: [],
                Service: [],
                Rental: [],
                Shop: [],
                Holiday: [],
                buildFromPayCodes: function ( allowed ) {
                    $scope.laborCodes.Job = [];
                    $scope.laborCodes.Service = [];
                    $scope.laborCodes.Rental = [];
                    $scope.laborCodes.Shop = [];
                    $scope.laborCodes.Holiday = [];
                    for ( var x = 0; x < allowed.length; x++ ) {
                        var payCode = PayCodes[allowed[x]];
                        if ( payCode == null ) {
                            continue;   //  Skip unknown codes
                        }
                        $scope.laborCodes[payCode.type].push(payCode);
                    }
                }
            };

            $scope.autocomplete = {
                jobs: Data.Jobs,
                service: Data.Service,
                rental: Data.Rental
            };

            $scope.view = {
                editingDetails: false,
                activeDay: "",
                canEdit: false,
                loading: false,
                getTotal: function ( entry ) {
                    return entry.Monday + entry.Tuesday + entry.Wednesday + entry.Thursday + entry.Friday + entry.Saturday + entry.Sunday;
                }
            };



        }
    ]);

