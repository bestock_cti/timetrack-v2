angular.module("TimeTrack")
    .service("OfficeProfileService", [
        "$http",
        "$q",
        function ( $http, $q ) {
            var api = {
                officeCoordinator: "",
                delegateOfficeCoordinator: "",
                branchManager: "",
                officeCode: "",
                currentEmployeeStatus: [],
                sendReminderEmail: false,
                reminderEmailTemplate: "",
                reminderEmailTime: "",
                today: ""
            };

            function set( key, value ) {
                sessionStorage.setItem(key, value);
            }
            function get( key ) {
                return sessionStorage.getItem(key)
            }

            function load() {
                api.officeCoordinator = get("officeCoordinator");
                api.delegateOfficeCoordinator = get("delegateOfficeCoordinator");
                api.branchManager = get("branchManager");
                api.officeCode = get("officeCode");
                api.currentEmployeeStatus = JSON.parse(get("currentEmployeeStatus") || "[]");
                api.sendReminderEmail = get("sendReminderEmail") == "true";
                api.reminderEmailTemplate = get("reminderEmailTemplate");
                api.reminderEmailTime = get("reminderEmailTime");
                api.today = get("today");
                console.log("loaded_api", api);
            }
            load();

            api.managerLoggedIn = function ( ) {
                var deferred = $q.defer();

                //   load this profile
                $http.get('/api/manager')
                    .then(function ( success ) {
                        var ref = success.data;
                        //set("employeeId", employee);
                        //set("username", username);

                        set("officeCoordinator", ref.OfficeCoordinator);
                        set("delegateOfficeCoordinator", ref.DelegateOfficeCoordinator);
                        set("branchManager", ref.BranchManager);
                        set("officeCode", ref.OfficeCode);
                        set("currentEmployeeStatus", JSON.stringify(ref.CurrentEmployeeStatus));
                        set("sendReminderEmail", ref.SendReminderEmail.toString());
                        set("reminderEmailTemplate", ref.ReminderEmailTemplate);
                        set("reminderEmailTime", ref.ReminderEmailTime);
                        set("today", ref.Today);
                        localStorage.setItem("FirstName", ref.FirstName);
                        localStorage.setItem("LastName", ref.LastName);
                        load();
                        deferred.resolve();
                    }, function ( err ) {
                        console.log("Error Loading User Profile!", err);
                        api.officeCoordinator = null;
                        api.delegateOfficeCoordinator = null;
                        api.branchManager = null;
                        api.officeCode = null;
                        api.currentEmployeeStatus = null;
                        api.sendReminderEmail = null;
                        api.reminderEmailTemplate = null;
                        api.reminderEmailTime = null;
                        api.today = null;
                    });

                return deferred.promise;
            };
            return api;
        }
    ]);

