angular.module("TimeTrack")
    .service("Data", [
        "$http",
        "$rootScope",
        function ( $http, $rootScope ) {
            var cache = {
                loadCount: 0,
                Rental: [],
                Jobs: [],
                Service: [],
                refresh: function () {
                    load();
                }
            };
            var api = {};

            function load () {
                $http.get('/api/data')
                    .then(function ( success ) {
                        api.Rental = success.data.Rental;
                        api.Jobs = success.data.Jobs;
                        api.Service = success.data.Service;
                        sessionStorage.setItem("DataLoaded", "true");
                        sessionStorage.setItem("Jobs", JSON.stringify(api.Jobs));
                        sessionStorage.setItem("Service", JSON.stringify(api.Service));
                        sessionStorage.setItem("Rental", JSON.stringify(api.Rental));

                        $rootScope.$broadcast("data-update");
                    }, function ( err ) {
                    });
            }
            if ( !sessionStorage.getItem("DataLoaded") ) {
                load();
            }
            return api = {
                Jobs: JSON.parse(sessionStorage.getItem("Jobs") || "[]"),
                Rental: JSON.parse(sessionStorage.getItem("Rental") || "[]"),
                Service:JSON.parse(sessionStorage.getItem("Service") || "[]")
            };
        }
    ]);

