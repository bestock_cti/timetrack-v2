angular.module("TimeTrack")
    .service("PayCodes", function () {

        function buildPayCode ( lbl, value, type ) {
            return {
                label: lbl,
                value: value,
                type: type
            };
        }

        return {
            "AP": buildPayCode("Audio Programming", "AudioProgramming", "Job"),
            "CP": buildPayCode("Control Programming", "ControlProgramming", "Job"),
            "DRF": buildPayCode("Drafting", "Drafting", "Job"),
            "ENG": buildPayCode("Engineering", "Engineering", "Job"),
            "FS": buildPayCode("Field Service", "FieldService", "Service"),
            "HOL": buildPayCode("Holiday", "Holiday", "Holiday"),
            "IHCP": buildPayCode("Ctrl Prgm In House", "InHouseCtrlPrgm", "Job"),
            "INH": buildPayCode("Install In House", "", "Job"),
            "INO": buildPayCode("Install On Site", "", "Job"),
            "ITL": null,//buildPayCode("", "", ""),    // Mystery Code #1
            "OSCP": buildPayCode("Ctrl Prgm On Site", "OnSiteCtrlPrgm", "Job"),
            "PER": buildPayCode("Personal/Vacation", "PersonalVacation", "Holiday"),
            "PM": buildPayCode("Project Management", "ProjectManagement", "Job"),
            "PRG": null,//buildPayCode("", "", ""),    //  Mystery Code #2
            "REN": buildPayCode("Rental", "Rental", "Rental"),
            "SHOP": buildPayCode("Shop Time", "Shop Time", "Shop"),
            "TPL": buildPayCode("Touch Panel Design", "TouchPanelDesign", "Job"),
            "TRJ": buildPayCode("Travel Time", "DriveTime", "Job"),
            "UL":  null //buildPayCode("", "", "")      //  Mystery Code #3
        };
    });

