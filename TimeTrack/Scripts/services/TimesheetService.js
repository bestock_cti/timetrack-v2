angular.module("TimeTrack")
    .service("TimesheetService", [
        '$http',
        '$q',
        function ( $http, $q ) {

            var api = {
                getMergedTimesheet: function ( employeeId ) {
                    var promise = $q.defer();

                    $http.get('/api/timesheet?employeeId=' + employeeId)
                        .then(function ( success ) {
                            promise.resolve(success.data);
                        }, function ( error ) {
                            promise.reject(error);
                        });

                    return promise.promise;
                },
                getFrozenTimesheet: function ( employeeId, weekEnding) {
                    var promise = $q.defer();

                    $http.get('/api/timesheet?employeeId=' + employeeId + '&weekEnding=' + weekEnding)
                        .then(function ( success ) {
                            promise.resolve(success.data);
                        }, function ( error ) {
                            promise.reject(error);
                        });

                    return promise.promise;
                },
                setModifiedEntries: function ( employeeId, modifiedEntries ) {
                    var promise = $q.defer();

                    $http.post('/api/timesheet?employeeId=' + employeeId, modifiedEntries )
                        .then(function ( success ) {
                            promise.resolve(success.data);
                        }, function ( error ) {
                            promise.reject(error);
                        });

                    return promise.promise;
                }
            };

            return api;
        }
    ]);





