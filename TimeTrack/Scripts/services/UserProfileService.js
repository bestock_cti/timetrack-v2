﻿angular.module("TimeTrack")
    .service("UserProfileService", [
        "$http",
        "$q",
        function ( $http, $q ) {
            var api = {
                employeeId: null,
                username: null,
                firstName: null,
                lastName: null,
                payCodes: [],
                dailySaveRequired: false,
                isManager: false,
                officeCode: null
            };

            function set( key, value ) {
                sessionStorage.setItem(key, value);
            }
            function get( key ) {
                return sessionStorage.getItem(key)
            }

            function load() {
                api.employeeId = get("employeeId");
                api.username = get("username");
                api.firstName = get("firstName");
                api.lastName = get("lastName");
                api.payCodes = JSON.parse(get("payCodes") || "[]");
                api.dailySaveRequired = get("dailySaveRequired") == "true";
                api.isManager = get("isManager") == "true";
                api.officeCode = get("officeCode");
                console.log("loaded_api", api);
            }
            load();

            api.userLoggedIn = function ( username, employee ) {
                var deferred = $q.defer();

               //   load this profile
                $http.get('/api/userprofile?employeeId=' + employee)
                    .then(function ( success ) {
                        var ref = success.data;
                        set("employeeId", employee);
                        set("username", username);
                        set("firstName", ref.FirstName);
                        set("lastName", ref.LastName);
                        set("dailySaveRequired", ref.DailySaveRequired.toString());
                        set("isManager", ref.IsManager.toString());
                        set("payCodes", JSON.stringify(ref.PayCodes));
                        set("officeCode", ref.OfficeCode);
                        localStorage.setItem("FirstName", ref.FirstName);
                        localStorage.setItem("LastName", ref.LastName);
                        load();
                        deferred.resolve();
                    }, function ( err ) {
                        console.log("Error Loading User Profile!", err);
                        api.employeeId = null;
                        api.username = null;
                        api.firstName = null;
                        api.lastName = null;
                        api.dailySaveRequired = false;
                        api.isManager = false;
                        api.payCodes = [];
                        api.officeCode = null;
                        deferred.reject();
                    });

                return deferred.promise;
            };
            return api;
        }
    ]);