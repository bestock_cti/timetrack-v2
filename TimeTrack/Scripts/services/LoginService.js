﻿angular.module("TimeTrack")
    .service("LoginService", [
        "$http",
        "$q",
        "$location",
        function ( $http, $q, $location ) {

            var loginDurationTimer = null,  // TODO make a timer to send the duration back to the server
                loginId = null;             // TODO attach an ID to each login record and use that to save the duration with the login timestamp


            function send ( evt ) {

            }


            return {
                forceLoggedIn: function () {
                    $http.get('/api/Login')
                        .then(function ( success ) {
                            if ( !success.data) {
                                $location.path("/login");
                            }
                            else {

                            }
                        }, function ( err ) {
                            $location.path("/login");
                        });
                },
                login: function ( username, password, rememberme ) {
                    var deferred = $q.defer();

                    $http.post('/api/Login', { Username: username, Password: password, RememberMe: rememberme })
                        .then(function ( success ) {
                            deferred.resolve(success.data);
                        }, function ( failure) {
                            deferred.reject(failure);
                        });

                    return deferred.promise;
                },
                stickyLogin: function ( username, secret ) {
                    var deferred = $q.defer();

                    $http.post('/api/Login?isSticky=true', { Username: username, Secret: secret })
                        .then(function ( success ) {
                            deferred.resolve(success.data);
                        }, function ( failure) {
                            deferred.reject(failure);
                        });

                    return deferred.promise;
                },
                logout: function ( ) {

                    if ( localStorage.getItem("Secret") != null ) {
                        if ( !confirm("You have 'Remember Me' enabled and logging out will force you to login again next time.\r\n    Do you want to continue?") ) {
                            return;
                        }
                    }

                    $http.delete('/api/Login?secret=' + localStorage.getItem("Secret"))
                        .then(function (){
                            localStorage.clear();
                            sessionStorage.clear();
                            $location.path("/login");
                        }, function ( err ) {
                            //  huh?
                            throw err;
                        });
                }
            };
        }
    ]);