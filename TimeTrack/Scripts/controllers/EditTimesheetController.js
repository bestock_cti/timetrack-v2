﻿angular.module("TimeTrack")
    .controller("EditTimesheetDemoController", [
        "$scope",
        function ( $scope ) {
            $scope.DemoOptions = {
                steps: [
                    {
                        element: "#demoStep1",
                        intro: "Welcome to the new TimeTrack!"
                    }
                ]
            }
        }
    ])
    .controller("EditTimesheetController", [
        "$scope",
        "UserProfileService",
        "LoginService",
        "TimesheetService",
        function ( $scope, UserProfileService, LoginService, TimesheetService ) {

            LoginService.forceLoggedIn();

            $scope.originalEntries = [];
            $scope.modifiedEntries = [];


            $scope.options = {
                activeDay: "",
                canEdit: true,
                loading: false,
                allowedPayCodes: UserProfileService.payCodes
            };

            $scope.$watch('UserProfileService', function ( n ) {
                $scope.options.allowedPayCodes = UserProfileService.payCodes;
                $scope.currentEmployee.id = UserProfileService.employeeId;
                $scope.currentEmployee.username = UserProfileService.username;
                $scope.currentEmployee.firstName = UserProfileService.firstName;
                $scope.currentEmployee.lastName = UserProfileService.lastName;

                if ($scope.currentEmployee.id == null || $scope.currentEmployee.id == "")
                    return;

                $scope.options.loading = true;
                TimesheetService.getMergedTimesheet($scope.currentEmployee.id)
                    .then(function ( timesheet ) {
                        $scope.originalEntries = timesheet.Entries;
                        $scope.options.activeDay = timesheet.CurrentWeekDay;
                        $scope.options.loading = false;
                    }, function ( err ) {
                        console.log("ERROR ", err);
                    });
            }, true);

            $scope.add = {
                job: function () {
                    $scope.originalEntries.push({
                        Type: "Job",
                        SubType: "",
                        Id: "",
                        Customer: "",
                        Note: "",
                        Monday: 0,
                        Tuesday: 0,
                        Wednesday: 0,
                        Thursday: 0,
                        Friday: 0,
                        Saturday: 0,
                        Sunday: 0,
                        Frozen: false,
                        unsaved: true
                    });
                },
                service: function () {
                    $scope.originalEntries.push({
                        Type: "Service",
                        SubType: "",
                        Id: "",
                        Customer: "",
                        Note: "",
                        Monday: 0,
                        Tuesday: 0,
                        Wednesday: 0,
                        Thursday: 0,
                        Friday: 0,
                        Saturday: 0,
                        Sunday: 0,
                        Frozen: false,
                        unsaved: true
                    });
                },
                rental: function () {
                    $scope.originalEntries.push({
                        Type: "Rental",
                        SubType: "",
                        Id: "",
                        Customer: "",
                        Note: "",
                        Monday: 0,
                        Tuesday: 0,
                        Wednesday: 0,
                        Thursday: 0,
                        Friday: 0,
                        Saturday: 0,
                        Sunday: 0,
                        Frozen: false,
                        unsaved: true
                    });
                },
                shop: function () {
                    $scope.originalEntries.push({
                        Type: "Shop",
                        SubType: "",
                        Id: "",
                        Customer: "",
                        Note: "",
                        Monday: 0,
                        Tuesday: 0,
                        Wednesday: 0,
                        Thursday: 0,
                        Friday: 0,
                        Saturday: 0,
                        Sunday: 0,
                        Frozen: false,
                        unsaved: true
                    });
                },
                holiday: function () {
                    $scope.originalEntries.push({
                        Type: "Holiday",
                        SubType: "Holiday",
                        Id: "",
                        Customer: "",
                        Note: "",
                        Monday: 0,
                        Tuesday: 0,
                        Wednesday: 0,
                        Thursday: 0,
                        Friday: 0,
                        Saturday: 0,
                        Sunday: 0,
                        Frozen: false,
                        unsaved: true
                    });
                },
                personal: function () {
                    $scope.originalEntries.push({
                        Type: "Holiday",
                        SubType: "PersonalVacation",
                        Id: "",
                        Customer: "",
                        Note: "",
                        Monday: 0,
                        Tuesday: 0,
                        Wednesday: 0,
                        Thursday: 0,
                        Friday: 0,
                        Saturday: 0,
                        Sunday: 0,
                        Frozen: false,
                        unsaved: true
                    });
                }
            };

            $scope.currentEmployee = {
                id: "",
                username: "",
                firstName: "",
                lastName: "",
                logout: function () {
                    LoginService.logout();
                }
            };

            $scope.feature = {
                save: true,
                reportIssue: true,
                import: false,
                export: false
            };
            $scope.types = {
                job: true,
                service: true,
                rental: true,
                shop: true,
                holiday: true
            };

            $scope.save = function () {
                TimesheetService.setModifiedEntries($scope.currentEmployee.id, $scope.modifiedEntries)
                    .then(function ( newTimesheet ) {
                        $scope.originalEntries = newTimesheet.Entries;
                    }, function ( err ) {
                        console.log("Error updating timesheet!");
                    });
            };

        }
    ]);