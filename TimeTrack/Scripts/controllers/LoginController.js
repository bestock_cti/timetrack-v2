﻿angular.module("TimeTrack")
    .controller("LoginController", [
        "$scope",
        "LoginService",
        "UserProfileService",
        "OfficeProfileService",
        "$location",
        function ( $scope, LoginService, UserProfileService, OfficeProfileService, $location ) {

            var stickyCreds = {
                username: localStorage.getItem("Username"),
                secret: localStorage.getItem("Secret"),
                firstName: localStorage.getItem("FirstName"),
                lastName: localStorage.getItem("LastName")
            };

            $scope.sticky = {
                firstName: stickyCreds.firstName,
                lastName: stickyCreds.lastName,
                username: stickyCreds.username,
                notYou: function () {
                    localStorage.removeItem("Username");
                    localStorage.removeItem("Secret");
                    localStorage.removeItem("FirstName");
                    localStorage.removeItem("LastName");
                    $scope.login.noSticky = true;
                },
                login: function () {
                    LoginService.stickyLogin(stickyCreds.username, stickyCreds.secret)
                        .then(function ( obj ) {
                            goToCorrectPane(obj.EmployeeId, stickyCreds.username);
                        }, function ( err ) {
                            console.log("error!", err);
                            $scope.login.noSticky = true;
                            $scope.login.username = stickyCreds.username;
                            $scope.login.rememberme = true;
                            $scope.login.hasError = true;
                            $scope.login.errorMessage = "Remember Me expired, login to reset!";
                        });
                }
            };

            function goToCorrectPane(employeeId, username) {
                UserProfileService.userLoggedIn(username, employeeId )
                    .then(function () {
                        if ( !UserProfileService.isManager ) {
                            return $location.path('/edit-timesheet');
                        }

                        if ( UserProfileService.isManager ) {
                            OfficeProfileService.managerLoggedIn()
                                .then(function () {
                                    return $location.path('/manager-panel');
                                }, function ( err ) {
                                    alert("Error Loading OfficeProfile");
                                });
                        }
                    }, function ( err ) {
                        alert("Error Loading UserProfile!");
                    });
            }

            $scope.login = {
                username: "",
                password: "",
                rememberme: false,
                noSticky: !(!!stickyCreds.username && !!stickyCreds.secret ),
                validateUsername: function () {
                    if ( $scope.login.username.indexOf("@") > -1 ) {
                        console.log("haserror");
                        $scope.login.errorMessage = "Login should not be an email!";
                        return $scope.login.hasError = true;
                    }
                    console.log('noerror');
                    $scope.login.errorMessage = "";
                    return $scope.login.hasError = false;
                },
                errorMessage: "",
                hasError: false,
                submit: function () {

                    //  Creates the Session details needed to keep the user on /edit-timesheet or /manager-panel
                    LoginService.login($scope.login.username, $scope.login.password, $scope.login.rememberme)
                        .then(function ( obj ) {

                            if ( $scope.login.rememberme ) {
                                //  store secret
                                localStorage.setItem("Username", $scope.login.username);
                                localStorage.setItem("Secret", obj.Secret);
                            }

                            goToCorrectPane(obj.EmployeeId, $scope.login.username);
                        }, function ( err ) {
                            $scope.login.errorMessage = "Error Logging In!";
                            $scope.login.hasError = true;
                            console.log("LoginError>", err);
                        });
                }
            };

        }
    ]);