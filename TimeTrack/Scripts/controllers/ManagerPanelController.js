﻿angular.module("TimeTrack")
    .controller("ManagerPanelController", [
        "$scope",
        "UserProfileService",
        "OfficeProfileService",
        "LoginService",
        "TimesheetService",
        function ( $scope, UserProfileService, OfficeProfileService, LoginService, TimesheetService ) {

            LoginService.forceLoggedIn();
            OfficeProfileService.managerLoggedIn()
                .then(function () {
                    $scope.employee.list = OfficeProfileService.currentEmployeeStatus;

                });

            $scope.view = {
                dashboard: true,
                options: false,
                employeeSelected: false,
                showWeekend: false,
                timesheet: false,
                logins: false,
                backups: false,
                setView: function ( id ) {
                    $scope.view.dashboard = false;
                    $scope.view.options = false;
                    $scope.view.timesheet = false;
                    $scope.view.logins = false;
                    $scope.view.backups = false;
                    $scope.view[id] = true;
                },
                employeeSelectedChange: function () {
                    if ( !$scope.view.employeeSelected ) {
                        $scope.view.timesheet = false;
                        $scope.view.logins = false;
                        $scope.view.backups = false;
                    }
                },
                setEmployeeSelected: function ( selected ) {
                    $scope.view.employeeSelected = selected;
                    if ( !selected ) {
                        $scope.view.timesheet = false;
                        $scope.view.logins = false;
                        $scope.view.backups = false;
                        return;
                    }
                    $scope.timesheet.options.loading = true;
                    TimesheetService.getMergedTimesheet($scope.employee.id)
                        .then(function ( timesheet ) {
                            $scope.timesheet.entries = timesheet.Entries;
                            $scope.timesheet.modified = [];
                            $scope.timesheet.options.loading = false;
                        }, function ( error ) {
                            console.log("Error fetching timesheet", error);
                        });
                }
            };

            $scope.timesheet = {
                entries: [],
                modified: [],
                options: {
                    activeDay: OfficeProfileService.today,
                    canEdit: true,
                    loading: false,
                    allowedPayCodes: UserProfileService.payCodes
                },
                save: function () {
                    TimesheetService.setModifiedEntries($scope.view.employeeSelected, $scope.timesheet.modified)
                        .then(function ( timesheet ) {
                            $scope.timesheet.entries = timesheet.Entries;
                            $scope.timesheet.modified = [];
                        }, function ( error ) {
                            console.log("Error saving modified entries!", error);
                        });
                }
            };

            $scope.employee = {
                list: OfficeProfileService.currentEmployeeStatus,
                id: null,
                select: function ( id ) {
                    if ( $scope.employee.id == id ) {
                        $scope.view.setEmployeeSelected(false);
                        return $scope.employee.id = null;
                    }
                    $scope.employee.id = id;
                    $scope.view.setEmployeeSelected(true);
                }
            };

            $scope.logins = {
                list: [],
                load: function () {

                }
            };
        }
    ]);