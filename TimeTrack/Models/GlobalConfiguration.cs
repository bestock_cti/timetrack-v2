﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class GlobalConfiguration
    {

        public List<string> AdminUsers { get; set; }
        public DateTime ReminderEmailTime { get; set; }
        public string ReminderEmailTemplate { get; set; }
        
    }
}