﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class ActiveIdsList
    {               
        public List<JobDetail> Jobs { get; set; }
        public List<ServiceDetail> Service { get; set; }
        public List<string> Rental { get; set; }
    }
}