﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Services;

namespace TimeTrack.Models
{
    public class OfficeProfile
    {
        public string OfficeCoordinator { get; set; }
        public string BranchManager { get; set; }    

        public List<EmployeeTimesheetStatus> CurrentEmployeeStatus { get; set; }
        public Dictionary<string, List<TimeEntry>> MigrationProblems { get; set; }
        public bool CanMigrate { get; set; }

        public bool SendReminderEmail { get; set; }
        public DateTime ReminderEmailTime { get; set; }
        public string ReminderEmailTemplate { get; set; }  
        public DayOfWeek Today { get; set; }

    }
}