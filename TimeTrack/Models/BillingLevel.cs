﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public enum BillingLevel
    {
        Full = 1,
        Half = 2,
        NotBilled = 3
    }        
}