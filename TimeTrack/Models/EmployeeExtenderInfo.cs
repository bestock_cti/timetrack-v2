﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class EmployeeExtenderInfo
    {
        public BillingLevel BillingCode { get; set; }
        public bool DailySaveRequired { get; set; }
        public string AltLoginId { get; set; }
    }
}

