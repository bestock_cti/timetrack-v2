﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Services;

namespace TimeTrack.Models
{
    public class UserProfile
    {
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool DailySaveRequired { get; set; }
        public bool IsManager { get; set; }
        public string Office { get; set; }
        public bool OfficeCoordinator { get; set; }
        public bool BranchManager { get; set; }
        public string AltLoginId { get; set; }
        public bool Excluded { get; set; }
        public BillingLevel BillingLevel { get; set; }

        public decimal PayRate(string payCode) 
        {
            return GpSqlService.GetPayRateForEmployee(EmployeeId, payCode);              
        }
        public List<string> PayCodes { get; set; }


        public UserProfile() { } 
    }
}