﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class MigrationError
    {
        public string EmployeeId { get; set; }
        public TimeEntry RelatedEntry { get; set; }
        public string Details { get; set; }
        public DateTime Timestamp { get; set; }
    }
}