﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class LoginRecord
    {
        public DateTime Timestamp { get; set; }
        public bool CleanlyClosed { get; set; }
        public long SessionDuration { get; set; }
    }
}