﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class ServiceDetail
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Customer { get; set; }
        public string Division { get; set; }
    }
}