﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class UniqueTimeEntry
    {
        public string Type { get; set; }
        public string Customer { get; set; }
        public string Id { get; set; }
        public string SubType { get; set; }

        public UniqueTimeEntry()
        {
        }
        public UniqueTimeEntry(string fromString)
        {
            string[] parts = fromString.Split(':');
            Type = parts[0];
            Customer = parts[1];
            Id = parts[2];
            SubType = parts[3];
        }

        public override string ToString()
        {
            return Type + ":" + Customer + ":" + Id + ":" + SubType;
        }

    }
        
}