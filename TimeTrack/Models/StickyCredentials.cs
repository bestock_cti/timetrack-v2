﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class StickyCredentials
    {
        public string Username { get; set; }
        public string Secret { get; set; }
    }
}