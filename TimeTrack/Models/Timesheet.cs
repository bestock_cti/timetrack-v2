﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class Timesheet
    {
        public DateTime WeekEndingDate { get; set; }
        public string CurrentWeekDay { get; set; }
        public List<TimeEntry> Entries { get; set; }
        

        public Timesheet() { }
        

    }
}