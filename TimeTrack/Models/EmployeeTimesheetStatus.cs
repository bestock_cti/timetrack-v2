﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Models
{
    public class EmployeeTimesheetStatus
    {
        public string EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Decimal? TotalHours { get; set; }
        public bool DailySaveRequired { get; set; }
        public string Office { get; set; }

        public Decimal? Monday { get; set; }
        public Decimal? Tuesday { get; set; }
        public Decimal? Wednesday { get; set; }
        public Decimal? Thursday { get; set; }
        public Decimal? Friday { get; set; }
    }
}