﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Services;
using TimeTrack.Services.Proxy;

namespace TimeTrack.Models
{
    public class TimeEntry
    {
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Id { get; set; }
        public string Customer { get; set; }
        public bool Frozen { get; set; }    //  Should always be true comming from the server right?
                                                                                                  
        public Decimal? Monday { get; set; }
        public Decimal? Tuesday { get; set; }
        public Decimal? Wednesday { get; set; }
        public Decimal? Thursday { get; set; }
        public Decimal? Friday { get; set; }
        public Decimal? Saturday { get; set; }
        public Decimal? Sunday { get; set; }

        public string MondayNote { get; set; }
        public string TuesdayNote { get; set; }
        public string WednesdayNote { get; set; }
        public string ThursdayNote { get; set; }
        public string FridayNote { get; set; }
        public string SaturdayNote { get; set; }
        public string SundayNote { get; set; }

        public UniqueTimeEntry GetUniqueTimeEntry()
        {
            return new UniqueTimeEntry()
            {
                Type = Type,
                SubType = SubType,
                Customer = Customer,
                Id = Id
            };
        }

        public TimeEntry GetHalvedTimeEntry()
        {
            TimeEntry e = new TimeEntry()
            {
                Type = Type,
                SubType = SubType,
                Id = Id,
                Customer = Customer,
                Frozen = Frozen,
                Monday = Monday / 2,
                Tuesday = Tuesday / 2,
                Wednesday = Wednesday / 2,
                Thursday = Thursday / 2,
                Friday = Friday / 2,
                Saturday = Saturday / 2,
                Sunday = Sunday / 2,
                MondayNote = MondayNote,
                TuesdayNote = TuesdayNote,
                WednesdayNote = WednesdayNote,
                ThursdayNote = ThursdayNote,
                FridayNote = FridayNote,
                SaturdayNote = SaturdayNote,
                SundayNote = SundayNote,
            };

            return e;
        }

        public TimeEntry() { }
        public TimeEntry(List<TimeSheetTrx> entrySet)
        {
            //  TODO: Reimplement this TimeEntry(List<TimeSheetTrx>) for converting ProblemEntries on migrate

        }

        public static string BuildPreSaleQuarterId(string preSaleJobNumber)
        {
            //  TODO: ReImplement the presalesquarterid handling

            return "";
                // return String.Format(
                //      "J.{0}-PRE-{1}-{2}",
                //      OfficeProfile.GetNameFromCode(preSaleJobNumber.Substring(3, 2)),
                //      "Q" + ((int)Math.Floor((decimal)DateTime.Now.Month / 12)),
                //      DateTime.Now.Year.ToString()
                //  );
        }
        
    }
}