﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Services;

namespace TimeTrack.Models
{
    public class TimeType
    {
        public string Type { get; set; }
        public string SubType { get; set; }
        public string PayCode { get; set; }
        public string CostCode { get; set; }

        #region Time Type Definitions
        public static TimeType Drafting = new TimeType()
        {
            Type = "Job",
            SubType = "Drafting",
            PayCode = "DRF",
            CostCode = "1040"
        };
        public static TimeType AudioProgramming = new TimeType()
        {
            Type = "Job",
            SubType = "Audio Programming",
            PayCode = "AP",
            CostCode = "1140"
        };
        public static TimeType InHouseCtrlPrgm = new TimeType()
        {
            Type = "Job",
            SubType = "Control Programming In House",
            PayCode = "IHCP",
            CostCode = "1130"
        };
        public static TimeType OnSiteCtrlPrgm = new TimeType()
        {
            Type = "Job",
            SubType = "Control Programming On Site",
            PayCode = "OSCP",
            CostCode = "1130"
        };
        public static TimeType Engineering = new TimeType()
        {
            Type = "Job",
            SubType = "Engineering",
            PayCode = "ENG",
            CostCode = "1020"
        };
        public static TimeType InstallInHouse = new TimeType()
        {
            Type = "Job",
            SubType = "Install In House",
            PayCode = "INH",
            CostCode = "1010"
        };
        public static TimeType InstallOnSite = new TimeType()
        {
            Type = "Job",
            SubType = "Install On Site",
            PayCode = "INO",
            CostCode = "1010"
        };
        public static TimeType ProjectManagement = new TimeType()
        {
            Type = "Job",
            SubType = "Project Management",
            PayCode = "PM",
            CostCode = "1100"
        };
        public static TimeType TouchPanelDesign = new TimeType()
        {
            Type = "Job",
            SubType = "Touch Panel Design",
            PayCode = "TPL",
            CostCode = "1090"
        };
        public static TimeType FieldService = new TimeType()
        {
            Type = "ServiceTicket",
            SubType = "Field Service",
            PayCode = "FS",
            CostCode = null
        };
        public static TimeType Holiday = new TimeType()
        {
            Type = "Holiday",
            SubType = "Holiday",
            PayCode = "HOL",
            CostCode = null
        };
        public static TimeType PersonalVacation = new TimeType()
        {
            Type = "Holiday",
            SubType = "Personal/Vacation",
            PayCode = "PER",
            CostCode = null
        };
        public static TimeType Rental = new TimeType()
        {
            Type = "Rental",
            SubType = "Rental",
            PayCode = "REN",
            CostCode = null
        };
        public static TimeType DriveTime = new TimeType()
        {
            Type = "Job",
            SubType = "Drive Time",
            PayCode = "TRJ",
            CostCode = "1110"
        };
        public static TimeType ShopTime = new TimeType()
        {
            Type = "Shop",
            SubType = "Shop Time",
            PayCode = "SHOP",
            CostCode = null
        };
        #endregion
        private static Dictionary<string, TimeType> TypesByPayCode = new Dictionary<string, TimeType>()
        {
            { "DRF", Drafting },
            { "AP", AudioProgramming },
            { "IHCP", InHouseCtrlPrgm },
            { "OSCP", OnSiteCtrlPrgm },
            { "ENG", Engineering },
            { "INH", InstallInHouse },
            { "INO", InstallOnSite },
            { "PM", ProjectManagement },
            { "TPL", TouchPanelDesign },
            { "FS", FieldService },
            { "HOL", Holiday },
            { "PER", PersonalVacation },
            { "REN", Rental },
            { "TRJ", DriveTime },
            { "SHOP", ShopTime }
        };
        private static Dictionary<string, TimeType> TypesBySubType = new Dictionary<string, TimeType>()
        {
            { "Drafting", Drafting },
            { "Audio Programming", AudioProgramming },
            { "Control Programming In House", InHouseCtrlPrgm },
            { "Control Programming On Site", OnSiteCtrlPrgm },
            { "Engineering", Engineering },
            { "Install In House", InstallInHouse },
            { "Install On Site", InstallOnSite },
            { "Project Management", ProjectManagement },
            { "Touch Panel Design", TouchPanelDesign },
            { "Field Service", FieldService },
            { "Holiday", Holiday },
            { "Personal/Vacation", PersonalVacation },
            { "Rental", Rental },
            { "Drive Time", DriveTime },
            { "Shop Time", ShopTime }
        };

        

        public static TimeType FromPayCode(string payCode)
        {
            if (!TypesByPayCode.ContainsKey(payCode))
            {
                LoggingService.ConfigurationError(
                    "LaborType.FromPayCode",
                    "Unknown Pay Code [" + payCode + "]",
                    TypesByPayCode);
                return null;
            }
            return TypesByPayCode[payCode];
        }
        public static TimeType FromSubType(string subType)
        {
            if (!TypesBySubType.ContainsKey(subType))
            {
                LoggingService.ConfigurationError(
                    "LaborType.FromSubType",
                    "Unknown Sub Type [" + subType + "]",
                    TypesBySubType);
                return null;
            }
            return TypesBySubType[subType];
        }
    }
}