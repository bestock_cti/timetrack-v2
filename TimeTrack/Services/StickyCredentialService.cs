﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Services
{
    public class StickyCredentialService
    {

        private static TimeSpan SecretTTL = new TimeSpan(5, 0, 0, 0);    // 5 days 

        private static string generateUniqueString()
        {                    
            return Guid.NewGuid().ToString();
        }

        public static bool ValidSecret(string username, string secret)
        {
            IDatabase db = RedisService.GetStickyCredDB();

            if (!db.KeyExists(secret))
            {
                return false;
            }
            string sUser = db.StringGet(secret);
            if (sUser != username)
            {
                return false;
            }
            return true;
        }

        public static string GetSecret(string username)
        {
            IDatabase db = RedisService.GetStickyCredDB();

            string secret = generateUniqueString();

            db.StringSet(secret, username);
            db.KeyExpire(secret, SecretTTL);

            return secret;
        }

        public static void BurnSecret(string secret)
        {
            IDatabase db = RedisService.GetStickyCredDB();

            db.KeyDelete(secret);
        }
    }
}