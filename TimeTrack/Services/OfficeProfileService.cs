﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Models;
using TimeTrack.Services.Proxy;

namespace TimeTrack.Services
{
    public class OfficeProfileService
    {    
        public static bool CanMigrateByOfficeId(string officeId) 
        {
            


            return false;
        }



        public struct OfficeMigrationResult
        {
            public bool Success { get; set; }
            public string Message { get; set; }
            public Dictionary<string, List<TimeEntry>> UsersWithErrors { get; set; }
        }
        public static OfficeMigrationResult MigrateOffice(string managerId)
        {
            //  should go through and migrate this office's data

            List<string> ManagedEmployees = OrganizationService.GetManagedEmployees(managerId);
            DayOfWeek Today = DayOfWeek.Monday;
            DateTime WeekEndingDate = DateTime.Parse(TimesheetService.GetCurrentWeekEnding());
            Dictionary<string, List<TimeSheetTrx>> userProblemEntries = new Dictionary<string, List<TimeSheetTrx>>();

            foreach (string employeeId in ManagedEmployees)
            {
                List<TimeEntry> ModifiedEntries = TimesheetService.GetModifiedEntries(employeeId);

                if (ModifiedEntries.Count == 0)
                {

                    return new OfficeMigrationResult()
                    {
                        Success = false,
                        Message = "User Timesheet Not Done! " + employeeId
                    };
                }

                List<TimeSheetTrx> entriesWithErrors = GpSqlService.SetLiveTimesheetEntries(employeeId, WeekEndingDate, ModifiedEntries, Today);

                if (entriesWithErrors.Count > 0)
                {
                    userProblemEntries.Add(employeeId, entriesWithErrors);
                    continue;
                }

                TimesheetService.SetModifiedEntries(employeeId, new List<TimeEntry>());     //  totally remove those entries

            }

            List<TimeSheetTrx> problemEntries = GpSqlService.SetLiveTimesheetEntries("BESTOCK", DateTime.Parse("12/25/15"), TimesheetService.GetModifiedEntries("BESTOCK"), DayOfWeek.Monday);

            //  Stick problem entries into a list associated with the office and the today value

             


            //================================

            //  1.) pull all timesheets for the current office

            //  2.) Verify everyone has saved for the day

            //  2:onFailure) return BadRequest("Timesheets Not All Done!") if people are still missing time

            //  3.) iterate through each timesheet and insert new transactions into GP 

            //  4.) after inserting the entries, pull the batch and compare to be sure everything went in correctly
            //  4:onFailure) Add the employeeId and entries that failed to a list object that is returned in the request

            //  5.) Return migration results


            return new OfficeMigrationResult()
            {
                Success = true
            };
        }
                                     
        public static Dictionary<string, List<TimeEntry>> GetMigrationErrors(string officeId) 
        {
            IDatabase db = RedisService.GetDB();

            RedisValue val = db.HashGet(officeId, "MigrationErrors");
            if (val.HasValue == false)
            {
                return new Dictionary<string, List<TimeEntry>>();
            }
            return JsonConvert.DeserializeObject<Dictionary<string, List<TimeEntry>>>(val);
        }
        public static void ClearMigrationErrors(string officeId)
        {
            IDatabase db = RedisService.GetDB();

            db.HashDelete(officeId, "MigrationErrors");
        }

        public static DayOfWeek GetToday(string officeId)
        {
            IDatabase db = RedisService.GetDB();

            RedisValue val = db.HashGet(officeId, "Today");
            if (val.HasValue == false)
            {
                return DayOfWeek.Monday;    //fallback to monday if there isn't one setup?
            }

            return (DayOfWeek)Enum.Parse(typeof(DayOfWeek), val);
        }
        public static void SetToday(string officeId, DayOfWeek today)
        {
            IDatabase db = RedisService.GetDB();

            db.HashSet(officeId, "Today", today.ToString()); 
        }

        public static string GetReminderEmailTemplate(string officeName)
        {
            //  get the global one or return a special one if set
            IDatabase db = RedisService.GetDB();

            RedisValue val = db.HashGet(officeName, "ReminderEmailTemplate");
            if ( val.HasValue == false ) 
            {
                val = db.HashGet("GLOBAL", "ReminderEmailTemplate");
            }

            return val;
        }
        public static DateTime GetReminderEmailTime(string officeName)
        {
            IDatabase db = RedisService.GetDB();

            RedisValue val = db.HashGet(officeName, "ReminderEmailTime");
            if (val.HasValue == false)
            {
                val = db.HashGet("GLOBAL", "ReminderEmailTime");
            }

            return DateTime.Parse(val);
        }
        public static bool GetSendReminderEmail(string officeName)
        {
            IDatabase db = RedisService.GetDB();

            RedisValue val = db.HashGet(officeName, "SendReminderEmail");
            if (val.HasValue == false)
            {
                val = db.HashGet("GLOBAL", "SendReminderEmail");
            }

            return val.ToString().ToLower() == "true";
        }

        public static void SetReminderEmailTemplate(string officeName, string template)
        {
            IDatabase db = RedisService.GetDB();

            db.HashSet(officeName, "ReminderEmailTemplate", template);
        }
        public static void SetReminderEmailTime(string officeName, DateTime time)
        {
            IDatabase db = RedisService.GetDB();

            db.HashSet(officeName, "ReminderEmailTemplate", time.Ticks);
        }
        public static void SetSendReminderEmail(string officeName, bool send)
        {
            IDatabase db = RedisService.GetDB();

            db.HashSet(officeName, "ReminderEmailTemplate", send.ToString());
        }






        internal static OfficeProfile GetOfficeProfileByManager(string AuthenticatedEmployeeId)
        {
            throw new NotImplementedException();
        }

        internal static OfficeProfile GetOfficeProfileByEmployee(string employeeId)
        {
            throw new NotImplementedException();
        }
    }
}