﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Models;
using TimeTrack.Extensions;

namespace TimeTrack.Services
{
    public class CachedDataService
    {

        private static Dictionary<string, EmployeeTimesheetStatus> Cache_EmployeeTimesheetStatus = new Dictionary<string, EmployeeTimesheetStatus>();
        
        private static ActiveIdsList Cache_ActiveIdsList = new ActiveIdsList();
        private static DateTime LastCached = DateTime.MinValue;
        
        public static ActiveIdsList GetActiveIdLists()
        {                                                                            
            if (DateTime.Now.Subtract(LastCached) > new TimeSpan(0, 5, 0))
            {
                Cache_ActiveIdsList = new ActiveIdsList()
                {
                    Jobs = GpSqlService.GetOpenJobs(),
                    Service = GpSqlService.GetOpenService(),
                    Rental = GpSqlService.GetOpenRentals()
                };
                LastCached = DateTime.Now;
            }

            return Cache_ActiveIdsList;
        }
        public static EmployeeTimesheetStatus GetEmployeeStatus(string employeeId)
        {
            throw new NotImplementedException();    //TODO : implement this!!
        }
        public static List<EmployeeTimesheetStatus> GetEmployeeStatus(List<string> employeeIds)
        {
            List<EmployeeTimesheetStatus> list = new List<EmployeeTimesheetStatus>();

            foreach (string emp in employeeIds) 
            {
                list.Add(GetEmployeeStatus(emp));
            }

            return list;
        }

        public static void SetEmployeeStatus(EmployeeTimesheetStatus status)
        {
            
        }
        
    }
}