﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Services
{
    public class RedisService
    {
        private static ConnectionMultiplexer connection { get; set; }

        public static void Connect()
        {
            connection = ConnectionMultiplexer.Connect("localhost");
        }

        public static IDatabase GetCacheDb()
        {
            return connection.GetDatabase(5);
        }

        public static IDatabase GetStickyCredDB()
        {
            return connection.GetDatabase(4);
        }

        public static IDatabase GetLoggingDB()
        {
            return connection.GetDatabase(3);
        }
        public static IDatabase GetConfigurationDB()
        {
            return connection.GetDatabase(2);
        }
        public static IDatabase GetProfileDB()
        {
            return connection.GetDatabase(1);
        }
        public static IDatabase GetDB()
        {
            return connection.GetDatabase(0);
        }

    }
}