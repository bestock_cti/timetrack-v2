﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Models;

namespace TimeTrack.Services
{
	public class UserProfileService
	{
        public static bool IsAdminUser(string AuthenticatedUserId)
        {
            return ConfigurationService.GetGlobalConfiguration().AdminUsers.Contains(AuthenticatedUserId);
        }

        public static UserProfile GetUserProfile(string employeeId)
        {
            EmployeeExtenderInfo extenderInfo = GpSqlService.GetExtenderInfoForEmployee(employeeId);
            Employee employee = GpSqlService.GetEmployee(employeeId);
            string office = GpSqlService.GetOfficeForEmployee(employeeId);
            List<string> payCodes = GpSqlService.GetPayCodesForEmployee(employeeId);

            return new UserProfile()
            {
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                EmployeeId = employee.EmployeeId,
                AltLoginId = extenderInfo.AltLoginId,
                BillingLevel = extenderInfo.BillingCode,
                DailySaveRequired = extenderInfo.DailySaveRequired,
                BranchManager = false,              //  TODO: decide if BranchManager/OfficeCoordinator is useful to store on UserProfile 
                OfficeCoordinator = false,
                Office = office,
                Excluded = employee.Excluded,
                IsManager = false,
                PayCodes = payCodes
            };
        }
    }
}