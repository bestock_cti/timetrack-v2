﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Web;
using TimeTrack.Models;
using TimeTrack.Services.Proxy;
using System.Data;
using TimeTrack.Extensions;
using StackExchange.Redis;
using Newtonsoft.Json;

namespace TimeTrack.Services
{
    public class GpSqlService
    {
        //  ---[ Settings ]---
        public const string Company = "CTI";
       
        //  ---[ Private ]--- 
        private static readonly TimeTrackServiceSoapClient Client = new TimeTrackServiceSoapClient();   
        private static string verifiedConnectionString { get; set; }
        
        private static SqlConnection GetConnection()
        {
            var conn = new SqlConnection(verifiedConnectionString);
            conn.Open();
            return conn;
        }
      

        //  ---[ Public ]---
        public static void Connect(string connectionString)
        {
            var conn = new SqlConnection(connectionString);
            conn.Open();
            conn.RetrieveStatistics();  // Random method to execute something on the server and verify it's actually a valid connection
            conn.Dispose();
            verifiedConnectionString = connectionString;
        }
        private static T GetKeyWithDefault<T>(Dictionary<DayOfWeek, T> dictionary, DayOfWeek key, T defaultVal)
        {
            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            return defaultVal;
        }
        public static string SubstituteAltLogin(string id)
        {
            // TODO: if the cache doesn't contain an entry with id as the AltLoginId then return id
            return id;
        }






        public static EmployeeExtenderInfo GetExtenderInfoForEmployee(string employeeeId)
        {
            using (var conn = GetConnection())
            {
                string sql = @"select [Employee ID] as EmployeeId, [Billable Percent for Techs] as BillingCode, [Alt. Login ID] as AltLoginId from CTI_TECHBILL where [Employee Id] = '" + employeeeId + "'";

                var results = conn.Query<EmployeeExtenderInfo>(sql);

                var x = "";
            }



            return new EmployeeExtenderInfo()
            {
                BillingCode = BillingLevel.Full,
                DailySaveRequired = false,
                AltLoginId = ""
            };
        }


       /* public static BillingLevel GetBillingLevelForEmployee(string employeeId)
        {
            using (var conn = GetConnection())
            {
                const string sql = @"SELECT        EXT01103.Extender_Record_ID, 
                                EXT01103.TOTAL AS BillingCodeRaw, 
                                EXT01100.Extender_Key_Values_1 AS EmployeeId 
                                FROM            EXT01103 INNER JOIN
                                EXT01100 ON EXT01103.Extender_Record_ID = EXT01100.Extender_Record_ID
                                WHERE        (EXT01103.Field_ID = 177)";
                var x = conn.Query<BillingLevelSQLModel>(sql);
                
                foreach (BillingLevelSQLModel entry in x)
                {
                    if (entry.EmployeeId.Trim() == employeeId)
                    {
                        return entry.BillingCode;
                    }
                }

                return BillingLevel.Full;
            }
        }
             */
        public static List<string> GetPayCodesForEmployee(string employeeId)
        {
            using (var conn = GetConnection()) 
            {
                string sql = "select PAYRCORD from UPR00400 where EMPLOYID = '" + employeeId + "'";
                IEnumerable<string> result = conn.Query<string>(sql);
                List<string> cResult = new List<string>();
                foreach (string s in result)
                {
                    cResult.Add(s.Trim());
                }
                return cResult;
            }
        }
        
        //  Internal Use
        public static decimal GetPayRateForEmployee(string employeeId, string payCode)
        {
            string sql = "SELECT PAYRTAMT FROM WS90400 WHERE EMPLOYID = '" + employeeId + "' AND PAYRCORD = '" + payCode + "' AND PayType = 1 AND Inactive = 0";
            using (var conn = GetConnection())
            {
                var result = conn.Query<decimal>(sql);
                var lResult = result.ToList();
                if (!lResult.Any())
                {
                    throw new Exception("Cannot find any payrates for '" + employeeId + "' using paycode '" + payCode + "'");
                }
                return lResult.First();
            }
        }
        public static string GetOfficeForEmployee(string employeeId)
        {
            string sql = "SELECT Distinct [LOCATNID] FROM [CTI].[dbo].[cti_timetrack_labor_allocation] WHERE [Employee ID] = '" + employeeId + "'";

            using (var conn = GetConnection())
            {
                var x = conn.Query<string>(sql).ToList();
                if (x.Count == 0)
                {
                    return "UNKNOWN";
                }
                return x.First().Trim();
            }
        }
                
        public static List<JobDetail> GetOpenJobs()
        {
            string sql = "select RTRIM(WS_Job_Number) Id, RTRIM(WS_Job_Name) Description, RTRIM(CUSTNMBR) Customer, RTRIM(Divisions) Division from jc00102";
            using (var conn = GetConnection())
            {
                return conn.Query<JobDetail>(sql).ToList();
            }
        }
        public static List<string> GetOpenRentals()
        {
            string sql = "SELECT distinct CONTRACTID FROM OPENQUERY(R2, 'select CONTRACTID  from R2.REP_OrderHeaderView')";
            using (var conn = GetConnection())
            {
                return conn.Query<string>(sql).ToList();
            }

        }
        public static List<ServiceDetail> GetOpenService()
        {
            const string sql = "select RTRIM(CALLNBR) Id, RTRIM(SVCDESCR) Description, RTRIM(CUSTNMBR) Customer, RTRIM(OFFID) Division from Svc00200"; 
            using (var conn = GetConnection())
            {
                return conn.Query<ServiceDetail>(sql).ToList();
            }
        }
        
        
        public static Employee GetEmployee(string employeeId)
        {
            string sql = @"SELECT * 
                                RTRIM(FRSTNAME) as FirstName,
                                RTRIM(LASTNAME) as LastName,
                                RTRIM(EMPLOYID) as EmployeeId,
                                CASE WHEN JOBTITLE = 'SALES' THEN 1 ELSE 0 END AS Excluded
                           FROM UPR00100 WHERE INACTIVE = 0";

            using (var conn = GetConnection())
            {
                List<Employee> rList = conn.Query<Employee>(sql).ToList();
                if (!rList.Any())
                {
                    return new Employee();
                }
                return rList.First();
            }
        }
        public static string GetBranchManager(string office)
        {
            //  do lookup against Office extender table

            throw new NotImplementedException();
        }
        public static string GetOfficeCoordinator(string office)
        {
            //  do lookup against office extender table
            throw new NotImplementedException();
        }
        public static string GetManagedOffice(string employeeId)
        {
            //  do lookup against office extender table

            throw new NotImplementedException();
        }
        private static List<TimeSheetTrx> GetLiveBatchTransactions(string batchId)
        {
            var batchInfo = Client.GetBatchInfo(batchId, ConfigurationService.GpConnectionString).Tables[0];

            if (batchInfo.Rows.Count <= 0)
            {
                return new List<TimeSheetTrx>();
            }                  


            List<TimeSheetTrx> transactions = new List<TimeSheetTrx>(); 
            using (var conn = GetConnection())
            {     
                var dr = conn.ExecuteReader("SELECT * FROM WS10702 WHERE BACHNUMB = @BatchName", new { BatchName = batchId }, CommandType.Text);
                while (dr.Read())
                {
                    transactions.Add(new TimeSheetTrx(dr));
                }    
            }
            return transactions;                                                              
        }
        private static List<TimeSheetTrx> GetHistoryBatchTransactions(string batchId)
        {
            //  TODO: implement history lookup
            return new List<TimeSheetTrx>();
        }
        
        public static string BuildBatchId(string employeeId, DateTime weekEnding, DayOfWeek day)
        {
            //  BatchId can only be 15 characters :/
            //  Z + ? + #### + #  == 6 constants with 9 left for the employeeId :/

            //  ----[ Known Collisions ]----
            //      * KHAGEMANN = [ 'Kim Hagemann', 'Kim Hagemann2' ]
            //      * SBROCKMAN = [ 'Stacy Brockman', 'Stacy Brockmann' ]

            //  0 == sunday, 6 == saturday
            string id = employeeId.ToUpper();
            if (id.Length > 9)
            {
                id = id.Substring(0, 9);
            }

            return "Z" + id + weekEnding.Month.ToString("00") + weekEnding.Day.ToString("00") + (int)day;
        }
        
        public static List<TimeEntry> GetLiveTimesheetEntries(string employeeId, DateTime weekEnding)
        {
            Dictionary<string, List<TimeSheetTrx>> uniqueEntries = new Dictionary<string, List<TimeSheetTrx>>();
            List<TimeEntry> entries = new List<TimeEntry>();
                       
            
            //  Go through all weekdays and retrieve any transactions stored in GP
            List<DayOfWeek> weekdays = new List<DayOfWeek>() { DayOfWeek.Saturday, DayOfWeek.Sunday, DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday };

            //  While retrieving transactions, group them according to unique time entries (customer,laborcode,job/svctkt match)
            foreach (DayOfWeek day in weekdays)
            {
                string batchId = BuildBatchId(employeeId, weekEnding, day);
                List<TimeSheetTrx> transactions = GetLiveBatchTransactions(batchId);

                foreach (TimeSheetTrx trans in transactions)
                {
                    TimeType timeType = TimeType.FromPayCode(trans.UPRTRXCD);
                    string type = timeType.Type;
                    string subtype = TimeType.FromPayCode(trans.UPRTRXCD).SubType;
                    string customer = trans.User_Define_2;
                    string id = "";

                    switch (type) 
                    {
                        case "FieldService":
                            type = "Service";
                            id = trans.TRXDSCRN;
                            break;
                        case "Rental":
                            type = "Rental";
                            id = trans.TRXDSCRN;
                            break;
                        case "Shop":
                            type = "Shop";
                            break;
                        case "Holiday":
                        case "Vacation/Personal":
                            type = "Holiday";
                            break;
                        case "":
                            //error
                            break;
                        default:
                            type = "Job";
                            id = trans.WS_Job_Number;
                            break;
                    }

                    UniqueTimeEntry entry = new UniqueTimeEntry()
                    {
                        Type = type,
                        SubType = subtype,
                        Customer = customer,
                        Id = id
                    };
                    if (uniqueEntries.ContainsKey(entry.ToString()))
                    {
                        uniqueEntries[entry.ToString()].Add(trans);
                        continue;
                    }
                    uniqueEntries[entry.ToString()] = new List<TimeSheetTrx>() { trans };
                }                                         
            }

            //  Iterate through each unique time entry and collapse hours into the TimeEntry
            Dictionary<string, decimal> totalHoursPerDay = new Dictionary<string, decimal>()
            {
                {"Total", 0}
            };
            foreach (string entry in uniqueEntries.Keys)
            {
                Dictionary<DayOfWeek, decimal> hoursPerDay = new Dictionary<DayOfWeek, decimal>();
                Dictionary<DayOfWeek, string> commentPerDay = new Dictionary<DayOfWeek, string>();

                foreach (TimeSheetTrx trans in uniqueEntries[entry])
                {
                    DayOfWeek dayOfWeek = trans.WS_Transaction_Date.DayOfWeek;

                    commentPerDay[dayOfWeek] = trans.TRXDSCRN;

                    if (hoursPerDay.ContainsKey(dayOfWeek) == false)
                    {
                        hoursPerDay[dayOfWeek] = 0;
                    } 
                    if (totalHoursPerDay.ContainsKey(dayOfWeek.ToString()) == false)
                    {
                        totalHoursPerDay[dayOfWeek.ToString()] = 0;
                    }
                    //  adds all days together because C# doesn't have dynamic property access *grumble*, wrong days should all be 0
                    //  NOTE: the += is just incase multiple days go in for one entry for some reason. Shouldn't happen but with corrections who knows
                    decimal hours = ((trans.Saturday + trans.Sunday + trans.Monday + trans.Tuesday + trans.Wednesday + trans.Thursday + trans.Friday) / 100);
                    hoursPerDay[dayOfWeek] += hours;
                    totalHoursPerDay[dayOfWeek.ToString()] += hours;
                    totalHoursPerDay["Total"] += hours;
                }

                UniqueTimeEntry uEntry = new UniqueTimeEntry(entry);

                entries.Add(new TimeEntry()
                {
                    Type = uEntry.Type,
                    SubType = uEntry.SubType,
                    Id = uEntry.Id,
                    Customer = uEntry.Customer,
                    Frozen = true,                  //  This entry came from GP! do not allow any changes
                    MondayNote = GetKeyWithDefault(commentPerDay, DayOfWeek.Monday, ""),
                    Monday = GetKeyWithDefault(hoursPerDay, DayOfWeek.Monday, 0),         
                    TuesdayNote = GetKeyWithDefault(commentPerDay, DayOfWeek.Tuesday, ""),
                    Tuesday = GetKeyWithDefault(hoursPerDay, DayOfWeek.Tuesday, 0),          
                    WednesdayNote = GetKeyWithDefault(commentPerDay, DayOfWeek.Wednesday, ""),
                    Wednesday = GetKeyWithDefault(hoursPerDay, DayOfWeek.Wednesday, 0),         
                    ThursdayNote = GetKeyWithDefault(commentPerDay, DayOfWeek.Thursday, ""),
                    Thursday = GetKeyWithDefault(hoursPerDay, DayOfWeek.Thursday, 0),          
                    FridayNote = GetKeyWithDefault(commentPerDay, DayOfWeek.Friday, ""),
                    Friday = GetKeyWithDefault(hoursPerDay, DayOfWeek.Friday, 0),           
                    SaturdayNote = GetKeyWithDefault(commentPerDay, DayOfWeek.Saturday, ""),
                    Saturday = GetKeyWithDefault(hoursPerDay, DayOfWeek.Saturday, 0),        
                    SundayNote = GetKeyWithDefault(commentPerDay, DayOfWeek.Sunday, ""),
                    Sunday = GetKeyWithDefault(hoursPerDay, DayOfWeek.Sunday, 0)                    
                });
            }

            IDatabase db = RedisService.GetDB();
            db.StringSet("Totals:" + employeeId, JsonConvert.SerializeObject(totalHoursPerDay));

            return entries;
        }
        public static List<TimeEntry> GetHistoryTimesheetEntries(string employeeId, DateTime weekEnding)
        {
            //  same as live lookup but against history table
            throw new NotImplementedException();
        }
        
        public static List<TimeSheetTrx> SetLiveTimesheetEntries(string employeeId, DateTime weekEnding, List<TimeEntry> entries, DayOfWeek day)
        {
            //  only create TimeSheetTrx for entries matching the current day
            List<TimeSheetTrx> toMigrate = new List<TimeSheetTrx>();

            string batchId = BuildBatchId(employeeId, weekEnding, day);

            int seqNumber = 0;
            UserProfile profile = OrganizationService.GetUserProfile(employeeId);
            foreach (TimeEntry entry in entries)
            {
                //  check if the entry has hours for the current day
                string dayStr = day.ToString().ToUpper();
                decimal? hours = 0;
                switch (day)
                {
                    case DayOfWeek.Monday:
                        hours = entry.Monday;
                        break;
                    case DayOfWeek.Tuesday:
                        hours = entry.Tuesday;
                        break;
                    case DayOfWeek.Wednesday:
                        hours = entry.Wednesday;
                        break;
                    case DayOfWeek.Thursday:
                        hours = entry.Thursday;
                        break;
                    case DayOfWeek.Friday:
                        hours = entry.Friday;
                        break;
                    case DayOfWeek.Saturday:
                        hours = entry.Saturday;
                        break;
                    case DayOfWeek.Sunday:
                        hours = entry.Sunday;
                        break;
                }
                if (hours == null || hours == 0)
                {
                    continue;
                }

                TimeType timeType = TimeType.FromSubType(entry.SubType);
                decimal totalPayment = profile.PayRate(timeType.PayCode) * hours.Value;

                switch (profile.BillingLevel)
                {
                    case BillingLevel.Full:
                        //string employeeId, DayOfWeek day, int seqNumber, TimeEntry entry, DateTime weekEnding, bool jobCost

                        toMigrate.Add(new TimeSheetTrx(employeeId, day, seqNumber++, entry, weekEnding, true));
                        continue;
                    case BillingLevel.Half:
                        TimeEntry halfTime = entry.GetHalvedTimeEntry();

                        toMigrate.Add(new TimeSheetTrx(employeeId, day, seqNumber++, halfTime, weekEnding, true));
                        toMigrate.Add(new TimeSheetTrx(employeeId, day, seqNumber++, halfTime, weekEnding, false)); 
                        continue;
                    case BillingLevel.NotBilled:        
                        toMigrate.Add(new TimeSheetTrx(employeeId, day, seqNumber++, entry, weekEnding, false));
                        continue;
                }
            }

            List<TimeSheetTrx> TransactionsWithErrors = new List<TimeSheetTrx>();
            foreach (TimeSheetTrx trx in toMigrate)
            {
                string response = Client.InsertTransaction(trx, verifiedConnectionString);
                bool success = response.Split('|')[0] == "0";

                if (!success) 
                {
                    //  tell me about it :)
                    TransactionsWithErrors.Add(trx);
                    continue;
                }
            }


            return TransactionsWithErrors;
        }
        
        //  [DEPRECATED]
        public static List<Employee> GetEmployeeList()
        {                                                                                                        
            string sql = @"SELECT RTRIM(FRSTNAME) AS FirstName, RTRIM(LASTNAME) AS LastName, RTRIM(EMPLOYID) AS EmployeeID FROM UPR00100 WHERE INACTIVE = 0 ORDER BY LastName, FirstName";
            using (var conn = GetConnection())
            {
                return conn.Query<Employee>(sql).ToList();
            }
        }

        public static string DivisionForJob(string id)
        {
            throw new NotImplementedException();
        }
        public static string DivisionForService(string id)
        {
            throw new NotImplementedException();
        }
        public static string BuildPreSalesIdForQuarter(string id)
        {
            throw new NotImplementedException();
        }
        public static bool IsPreSalesJob(string jobNumber)
        {
            //  TODO : check if the job is a presales entry (does it exist in the cached job list)

            throw new NotImplementedException();
        }
        //  ===> Complete End <===
         


       
    }
}