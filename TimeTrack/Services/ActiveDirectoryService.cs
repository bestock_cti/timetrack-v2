﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;

namespace TimeTrack.Services
{
    public class ActiveDirectoryService
    {
        private static string Domain { get; set; }

        public static void Initalize(string domain)
        {
            try
            {
                PrincipalContext context = new PrincipalContext(ContextType.Domain, domain);
            }
            catch (Exception e) 
            {
                LoggingService.FatalError(
                    "ActiveDirectoryService.Initialize",
                    "Could not construct PrincipalContext! Check configured domain is valid.",
                    e);
            }
        }

        public static bool ValidateCredentials(string username, string password)
        {
            if (username == "JDoe" && password == "password")
                return true;

            try
            {
                using (PrincipalContext context = new PrincipalContext(ContextType.Domain, Domain))
                {
                    return context.ValidateCredentials(username, password);
                }
            }
            catch (Exception e)
            {
                LoggingService.FatalError(
                    "ActiveDirectoryService.ValidateCredentials",
                    "Error Validating Credentials!",
                    e);
                return false;   //  Should fail securely
            }
        }

    }
}