﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using TimeTrack.Models;
using TimeTrack.Extensions;

namespace TimeTrack.Services
{
    public class RedisInterface
    {
        private static ConnectionMultiplexer redisConnection { get; set; }
        private static string previousHostName { get; set; }
        public static void Connect(string hostName) 
        {
            previousHostName = hostName;
            redisConnection = ConnectionMultiplexer.Connect(hostName);
        }      
        private static IDatabase GetDB(int id)
        {
            if ( redisConnection == null ||  redisConnection.IsConnected == false ) {
                if ( previousHostName == null ) 
                {
                    throw new Exception("Database never connected! Setup hostname parameter");
                }
                try
                {
                    redisConnection = ConnectionMultiplexer.Connect(previousHostName);
                }
                catch (Exception e)
                {
                    using (SmtpClient client = new SmtpClient(ConfigurationService.MailServerHostname))
                    {
                        //  Sends an email containing the error information to me.
                        client.Send(new MailMessage("timetrackerror@conferencetech.com", "bestock@conferencetech.com", "DB Lost Connection!", 
                            "Got Exception on reconnect attempt. \r\n" + JsonConvert.SerializeObject(e)));

                        //  Sends an SMS that the database is down to my phone in case it happens off hours so I can get it started quickly
                        client.Send(new MailMessage("timetrackerror@conferencetech.com", "3143049825@tmomail.net", "", "Redis DB Down!"));
                    }
                    throw new Exception("Database Connection Down! Please restart 'redis-server.exe' on '" + previousHostName + "'");
                }
            }
            return redisConnection.GetDatabase(id);
        }

        
        public class TimesheetDB
        {
            private static int Id = 0;

            public static List<TimeEntry> GetModifiedEntries(string employeeId)
            {
                RedisValue val = GetDB(Id).StringGet(employeeId.NormalizeEmployeeId());

                if (val.HasValue == false)
                {
                    return new List<TimeEntry>();
                }

                return JsonConvert.DeserializeObject<List<TimeEntry>>(val);
            }
            public static void SetModifiedEntries(string employeeId, List<TimeEntry> entries)
            {
                GetDB(Id).StringSet(employeeId.NormalizeEmployeeId(), JsonConvert.SerializeObject(entries));
            }

            public static List<MigrationError> GetMigrationErrors(string office)
            {
                RedisValue val = GetDB(Id).StringGet(office + "MigrationErrors");

                if (val.HasValue == false)
                {
                    return new List<MigrationError>();
                }

                return JsonConvert.DeserializeObject<List<MigrationError>>(val);
            }
            public static void SetMigrationErrors(string office, List<MigrationError> errors)
            {
                if (errors.Count == 0)
                {
                    //  Remove key and ignore.
                    GetDB(Id).KeyDelete(office + "MigrationErrors");
                    return;
                }
                GetDB(Id).StringSet(office + "MigrationErrors", JsonConvert.SerializeObject(errors));
                File.WriteAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data") + office + ".migrationerrors-" + DateTime.Now.Ticks + ".json", JsonConvert.SerializeObject(errors));
            }
        }

        public class StickyCredentialsDB
        {
            private static int Id = 1;



            private static TimeSpan Secret_Lifespan = new TimeSpan(5, 0, 0, 0); // 5 days



            public static bool TestSecret(string username, string secret)
            {
                var db = GetDB(Id);

                if (db.KeyExists(secret) == false)
                {
                    //  Failed because of unknown secret!
                    return false;
                }

                if (db.StringGet(secret) != username.NormalizeEmployeeId())
                {
                    //  REALLY WEIRD!! valid secret but for wrong user :/ fail it!
                    return false;
                }

                //  valid secret for the correct user;
                return true;
            }

            public static string GenerateSecret(string username)
            {
                string secret = Guid.NewGuid().ToString();

                GetDB(Id).StringSet(secret, username.NormalizeEmployeeId());
                GetDB(Id).KeyExpire(secret, Secret_Lifespan);

                return secret;
            }

            public static void BurnSecret(string secret)
            {
                GetDB(Id).KeyDelete(secret);
            }
        }

        public class ConfigurationDB
        {
            private static int Id = 2;      



            private static string Default_GlobalReminderTime = "6:00 PM";
            private static string Default_GlobalReminderTemplate = "You haven't updated your timesheet yet today, be sure to get it done soon!";
            private static List<string> Default_AdminUsers = new List<string>() { "BESTOCK", "PRICHARDSON", "TBRAY" };



            public static string GetGlobalReminderTemplate()
            {
                RedisValue val = GetDB(Id).StringGet("GlobalReminderTemplate");

                if (val.HasValue == false)
                {
                    GetDB(Id).StringSet("GlobalReminderTemplate", Default_GlobalReminderTemplate);
                    return Default_GlobalReminderTemplate;
                }

                return val;
            }
            public static void SetGlobalReminderTemplate(string template)
            {
                GetDB(Id).StringSet("GlobalReminderTemplate", template);
            }

            public static DateTime GetGlobalReminderTime()
            {
                RedisValue val = GetDB(Id).StringGet("GlobalReminderTime");
                
                if ( val.HasValue == false ) {
                    GetDB(Id).StringSet("GlobalReminderTime", Default_GlobalReminderTime);
                    return DateTime.Parse(Default_GlobalReminderTime);
                }

                return DateTime.Parse(val);
            }
            public static void SetGlobalReminderTime(DateTime time)
            {
                GetDB(Id).StringSet("GlobalReminderTime", time.Hour + ":" + time.Minute);
            }

            public static List<string> GetAdminUsers()
            {
                List<string> adminUsers = Default_AdminUsers;  

                RedisValue val = GetDB(Id).StringGet("AdminUsers");

                if (val.HasValue == false)
                {
                    GetDB(Id).StringSet("AdminUsers", JsonConvert.SerializeObject(adminUsers));
                    return adminUsers;
                }

                adminUsers = JsonConvert.DeserializeObject<List<string>>(val);
                return adminUsers;
            }
            public static void SetAdminUsers(List<string> users)
            {
                //  TODO: normalize all id's in the list
                GetDB(Id).StringSet("AdminUsers", JsonConvert.SerializeObject(users));
            }

            public static string GetOfficeReminderTemplate(string office)
            {
                RedisValue val = GetDB(Id).StringGet(office + "ReminderTemplate");

                if (val.HasValue == false)
                {
                    string globalValue = GetGlobalReminderTemplate();
                    GetDB(Id).StringSet(office + "ReminderTemplate", globalValue);
                    return globalValue;
                }

                return val;
            }
            public static void SetOfficeReminderTemplate(string office, string template)
            {
                GetDB(Id).StringSet(office + "ReminderTemplate", template);
            }
              
            public static DateTime GetOfficeReminderTime(string office)
            {
                RedisValue val = GetDB(Id).StringGet(office + "ReminderTime");

                if (val.HasValue == false)
                {
                    DateTime globalValue = GetGlobalReminderTime();
                    GetDB(Id).StringSet(office + "ReminderTime", globalValue.Hour + ":" + globalValue.Minute);
                    return globalValue;
                }

                return DateTime.Parse(val);
            }
            public static void SetOfficeReminderTime(string office, DateTime time)
            {
                GetDB(Id).StringSet(office + "ReminderTime", time.Hour + ":" + time.Minute);
            }
        }

        public class LoggingDB
        {
            private static int Id = 3;



            //  TODO: implement this for something? :/
        }
    }
}