﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TimeTrack.Models;
using Dapper;
using TimeTrack.Extensions;


namespace TimeTrack.Services
{
    public class GpSqlInterface
    {
        public const string CompanyName = "TEST";   //  this affects the entire application
        public static void Connect(string connStr)
        {
            SqlConnection conn = new SqlConnection(connStr);

            conn.Open();
            conn.RetrieveStatistics();
            conn.Dispose();

            ConnectionString = connStr;
        }

        
        private static string ConnectionString { get; set; } 
        private static SqlConnection GetConnection()
        {
            SqlConnection conn = new SqlConnection(ConnectionString);

            return conn;
        }


        public static List<JobDetail> GetOpenJobs()
        {
            string sql = @"SELECT
                                RTRIM([WS_Job_Number) AS Id,
                                RTRIM([WS_Job_Name) AS Description,
                                RTRIM([CUSTNMBR]) AS Customer,
                                RTRIM([Divisions]) AS Division
                           FROM     JC00102";

            using (SqlConnection conn = GetConnection()) 
            {
                return conn.Query<JobDetail>(sql).ToList();
            }   
        }
        public static List<ServiceDetail> GetOpenService()
        {
            string sql = @"SELECT
                                RTRIM([CALLNBR]) AS Id,
                                RTRIM([SVCDSCR]) AS Description,
                                RTRIM([CUSTNMBR]) AS Customer,
                                RTRIM([OFFID]) AS Division
                           FROM     SVC00200";

            using (SqlConnection conn = GetConnection())
            {
                return conn.Query<ServiceDetail>(sql).ToList();
            }
        }
        public static List<string> GetOpenRental()
        {
            string sql = @"SELECT DISTINCT [CONTRACTID]
                           FROM OPENQUERY(R2, 'SELECT [CONTRACTID] FROM R2.REP_OrderHeaderView')";

            using (SqlConnection conn = GetConnection())
            {
                return conn.Query<string>(sql).ToList();
            }
        }


        public static decimal GetEmployeePayRate(string employeeId, string payCode)
        {
            string sql = @"SELECT   [PAYRTAMT]
                           FROM     WS90400
                           WHERE    
                                [EMPLOYID] = '" + employeeId.NormalizeEmployeeId() + @"'
                                AND
                                [PAYRCORD] = '" + payCode + @"'
                                AND
                                [PayType] = 1
                                AND
                                [Inactive] = 0";

            using (SqlConnection conn = GetConnection())
            {
                return conn.Query<decimal>(sql).ToList().FirstOrDefault();
            }
        }
        public static List<string> GetEmployeePayCodes(string employeeId)
        {
            string sql = @"SELECT   RTRIM([PAYRCORD])
                           FROM     UPR00400
                           WHERE    [EMPLOYID] = '" + employeeId.NormalizeEmployeeId() + "'";

            using (SqlConnection conn = GetConnection())
            {
                return conn.Query<string>(sql).ToList();
            }
        }
        public static EmployeeExtenderInfo GetEmployeeExtenderInfo(string employeeId)
        {
            string sql = @"SELECT 
                                [Employee ID]                   AS EmployeeId,
                                [Billable Percent for Techs]    AS BillingCode,
                                [Alt. Login ID]                 AS AltLoginId
                            FROM    CTI_TECHBILL
                            WHERE   [Employee ID] = '" + employeeId.NormalizeEmployeeId() + "'";

            using (SqlConnection conn = GetConnection())
            {
                return conn.Query<EmployeeExtenderInfo>(sql).ToList().FirstOrDefault();  
            }
        }
        public static Employee GetEmployee(string employeeId)
        {
            string sql = @"SELECT 
                                RTRIM([FRSTNAME]) AS FirstName,
                                RTRIM([LASTNAME]) AS LastName,
                                RTRIM([EMPLOYID]) AS EmployeeId,
                                CASE WHEN [JOBTITLE] = 'SALES' THEN 1 ELSE 0 END AS Excluded
                           FROM UPR00100 
                           WHERE 
                               [INACTIVE] = 0
                               AND
                               [EMPLOYID] = '" + employeeId.NormalizeEmployeeId() + "'";

            using (SqlConnection conn = GetConnection())
            {
                return conn.Query<Employee>(sql).ToList().FirstOrDefault();
            }
        }
        
        public static List<TimeSheetTrx> GetLiveTimesheetEntries(string batchId) {
            var batchInfo = Client.GetBatchInfo(batchId, ConnectionString).Tables[0];
            string SQL = @"SELECT * FROM WS10702 WHERE BACHNUMB = @BatchName";

            if ( batchInfo.Rows.Count <= 0 ) {
                //  There aren't any entries for this batch
                return new List<TimeSheetTrx>();    
            }

            List<TimeSheetTrx> transactions = new List<TimeSheetTrx>();
            using(SqlConnection conn = GetConnection()) {
                var dataReader = conn.ExecuteReader(SQL, new { BatchName = batchId }, CommandType.Text);
                while ( dataReader.Read() ) {
                    transactions.Add(new TimeSheetTrx(dataReader));
                }
            }
            return transactions;
        }
        public static List<TimeSheetTrx> GetHistoryTimesheetEntries(string batchId) {
            var batchInfo = Client.GetBatchInfo(batchId, ConnectionString).Tables[0];
            string SQL = @"SELECT * FROM WS30702 WHERE BACHNUMB = @BatchName";

            if ( batchInfo.Rows.Count <= 0 ) {
                //  There aren't any entries for this batch
                return new List<TimeSheetTrx>();    
            }

            List<TimeSheetTrx> transactions = new List<TimeSheetTrx>();
            using(SqlConnection conn = GetConnection()) {
                var dataReader = conn.ExecuteReader(SQL, new { BatchName = batchId }, CommandType.Text);
                while ( dataReader.Read() ) {
                    transactions.Add(new TimeSheetTrx(dataReader));
                }
            }
            return transactions;
        }
        public static List<TimeSheetTrx> SetLiveTimesheetEntries(string batchId, List<TimeSheetTrx> entries) {
           
        }

    }
}