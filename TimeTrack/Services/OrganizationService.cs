﻿using Microsoft.SharePoint.Client;
using SP = Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Models;
using System.IO;
using StackExchange.Redis;
using Newtonsoft.Json;

namespace TimeTrack.Services
{
    public class OrganizationService
    {
        public struct Office 
        {
            public string OfficeCoordinator { get; set; }
            public string DelegateOfficeCoordinator { get; set; }
            public string BranchManager { get; set; }
        }

        private static List<string> MasterUsers = new List<string>();
        private static Dictionary<string, Office> OfficeManagers = new Dictionary<string, Office>();
        private static Dictionary<string, List<string>> OfficeUsers = new Dictionary<string, List<string>>();

        private static Dictionary<string, UserProfile> UserProfiles = new Dictionary<string, UserProfile>();
                 
        private static string CacheRoot = System.Web.Hosting.HostingEnvironment.MapPath("~/Profiles");
        public static void RefreshUserPictureCache()
        {
            if (System.IO.File.Exists(CacheRoot + "/Pictures/loadtime.txt"))
            {
                string file = System.IO.File.ReadAllText(CacheRoot + "/Pictures/loadtime.txt");
                DateTime loadedTime;
                if ( DateTime.TryParse(file, out loadedTime) ) 
                {
                    TimeSpan age = DateTime.Now.Subtract(loadedTime);
                    if (age.TotalHours < 23)
                    {
                        //  The Cache is still too new (less than 23 hours old), skip this step
                        return;
                    }
                }
            } 

            ClientContext context = new ClientContext("http://onedrive");

            CamlQuery lookupQuery = new CamlQuery();
            lookupQuery.ViewXml = @"<View></View>";

            List list = context.Web.Lists.GetByTitle("User Photos");
            context.Load(list);
            context.ExecuteQuery();

            ListItemCollection listItems = list.GetItems(lookupQuery);
            context.Load(listItems);
            context.ExecuteQuery();

            Folder ProfilePictures = listItems.First().Folder;
            FileCollection files = ProfilePictures.Files;

            context.Load(files);
            context.ExecuteQuery();
                                                                                
            foreach (SP.File file in files)
            {
                string _ref = file.ServerRelativeUrl;
                string patternStr = "_LThumb.jpg";
                if (_ref.Substring(_ref.Length - patternStr.Length) != patternStr)
                {
                    continue;
                }
                string prefixStr = "/User Photos/Profile Pictures/";
                string employeeId = _ref.Substring(prefixStr.Length, (_ref.Length - prefixStr.Length) - patternStr.Length);

                ClientResult<Stream> data = file.OpenBinaryStream();

                context.Load(file);
                context.ExecuteQuery();

                if (data != null)
                {
                    int position = 1;
                    int bufferSize = 200000;
                    Byte[] readBuffer = new Byte[bufferSize];
                    string localFilePath = CacheRoot + "/Pictures/" + employeeId + ".jpg";
                    using (System.IO.Stream stream = System.IO.File.Create(localFilePath))
                    {
                        while (position > 0)
                        {
                            // data.Value holds the Stream
                            position = data.Value.Read(readBuffer, 0, bufferSize);
                            stream.Write(readBuffer, 0, position);
                            readBuffer = new Byte[bufferSize];
                        }
                        stream.Flush();
                    }
                }

            }

            //  write loadtime.txt
            System.IO.File.WriteAllText(CacheRoot + "/Pictures/loadtime.txt", DateTime.Now.ToString());
        }

        public static void CheckCachedProfiles()
        {
            if (System.IO.File.Exists(CacheRoot + "/loadtime.txt"))
            {
                string file = System.IO.File.ReadAllText(CacheRoot + "/loadtime.txt");
                DateTime loadedTime;
                if (DateTime.TryParse(file, out loadedTime))
                {
                    TimeSpan age = DateTime.Now.Subtract(loadedTime);
                    if (age.TotalHours < 23)
                    {
                        //  The Cache is still too new (less than 23 hours old), skip this step

                        if (OfficeUsers.Count == 0 && OfficeManagers.Count == 0 && UserProfiles.Count == 0)
                        {
                            //  load from the cache file                                                           

                            string cacheFile = System.IO.File.ReadAllText(CacheRoot + "/cache.json");
                            var cache = JsonConvert.DeserializeAnonymousType(cacheFile, new
                            {
                                UserProfiles = new Dictionary<string, UserProfile>(),
                                OfficeManagers = new Dictionary<string, Office>(),
                                OfficeUsers = new Dictionary<string, List<string>>(),
                                MasterUsers = new List<string>()
                            });

                            UserProfiles = cache.UserProfiles;
                            OfficeManagers = cache.OfficeManagers;
                            OfficeUsers = cache.OfficeUsers;
                            MasterUsers = cache.MasterUsers;

                            //  HACK : add fake manager on rebuild
                            UserProfiles.Add("JDOE", new UserProfile()
                            {
                                FirstName = "John",
                                LastName = "Doe",
                                EmployeeId = "JDOE",
                                IsManager = true
                            });
                        }

                        return;
                    }                                         
                }
            }
            LoadProfilesFromGP();
            //  HACK : add fake manager at start 
            UserProfiles.Add("JDOE", new UserProfile()
            {
                FirstName = "John",
                LastName = "Doe",
                EmployeeId = "JDOE",
                IsManager = true
            });
        }
        private static void LoadProfilesFromGP()
        {
         
        }


        public static dynamic GetOrgChart()
        {
            //  LEVEL 1)  Master Users

            //  LEVEL 2) Offices (manager/coordinator pair)
 
            //  LEVEL 3) Employees 

            //  STUB: string GetOrgChart()
            return new 
            { 
                MasterUsers = MasterUsers,
                OfficeUsers = OfficeUsers,
                OfficeManagers = OfficeManagers
            };
        }

        public static UserProfile GetUserProfile(string employeeId)
        {
            employeeId = employeeId.ToUpper();

            var profile = UserProfiles[employeeId];

            string managedOffice = (from office in OfficeManagers
                                where office.Value.BranchManager == employeeId || office.Value.OfficeCoordinator == employeeId
                                select office.Key).FirstOrDefault();

            if (managedOffice != "" && managedOffice != null)
            {
                profile.IsManager = true;
            }

            return profile;
        }
        private static Dictionary<string, Decimal?> GetHoursForDay(Dictionary<string, Decimal?> dictionary, DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Monday:
                    if (dictionary.ContainsKey("Monday") == false)
                    {
                        dictionary["Monday"] = null;
                    }
                    dictionary["Tuesday"] = null;
                    dictionary["Wednesday"] = null;
                    dictionary["Thursday"] = null;
                    dictionary["Friday"] = null;
                    return dictionary;
                case DayOfWeek.Tuesday:
                    if (dictionary.ContainsKey("Monday") == false)
                    {
                        dictionary["Monday"] = 0;
                    }       
                    if (dictionary.ContainsKey("Tuesday") == false)
                    {
                        dictionary["Tuesday"] = null;
                    }                        
                    dictionary["Wednesday"] = null;
                    dictionary["Thursday"] = null;
                    dictionary["Friday"] = null;
                    return dictionary;
                case DayOfWeek.Wednesday:   
                    if (dictionary.ContainsKey("Monday") == false)
                    {
                        dictionary["Monday"] = 0;
                    }
                    if (dictionary.ContainsKey("Tuesday") == false)
                    {
                        dictionary["Tuesday"] = 0;
                    }                
                    if (dictionary.ContainsKey("Wednesday") == false)
                    {
                        dictionary["Wednesday"] = null;
                    }             
                    dictionary["Thursday"] = null;
                    dictionary["Friday"] = null;
                    return dictionary;
                case DayOfWeek.Thursday:
                    if (dictionary.ContainsKey("Monday") == false)
                    {
                        dictionary["Monday"] = 0;
                    }                  
                    if (dictionary.ContainsKey("Tuesday") == false)
                    {
                        dictionary["Tuesday"] = 0;
                    }                                       
                    if (dictionary.ContainsKey("Wednesday") == false)
                    {
                        dictionary["Wednesday"] = 0;
                    }               
                    if (dictionary.ContainsKey("Thursday") == false)
                    {
                        dictionary["Thursday"] = null;
                    }                                
                    dictionary["Friday"] = null;
                    return dictionary;
                case DayOfWeek.Friday:                                      
                    if (dictionary.ContainsKey("Monday") == false)
                    {
                        dictionary["Monday"] = 0;
                    }
                    if (dictionary.ContainsKey("Tuesday") == false)
                    {
                        dictionary["Tuesday"] = 0;
                    }
                    if (dictionary.ContainsKey("Wednesday") == false)
                    {
                        dictionary["Wednesday"] = 0;
                    }                                      
                    if (dictionary.ContainsKey("Thursday") == false)
                    {
                        dictionary["Thursday"] = 0;
                    }                 
                    if (dictionary.ContainsKey("Friday") == false)
                    {
                        dictionary["Friday"] = null;
                    }
                    return dictionary;
            }
            return dictionary;
        }

        public static List<string> GetManagedEmployees(string employeeId)
        {
            string officeCode = (from office in OfficeManagers
                                 where office.Value.BranchManager == employeeId || office.Value.OfficeCoordinator == employeeId
                                 select office.Key).FirstOrDefault();

            if (OfficeUsers.ContainsKey(employeeId))
            {
                return OfficeUsers[officeCode];
            }
            return new List<string>();
        }
        public static OfficeProfile GetOfficeProfile(string employeeId)
        {
            employeeId = employeeId.ToUpper();

            IDatabase db = RedisService.GetDB();

            string officeCode = (from office in OfficeManagers
                                 where office.Value.BranchManager == employeeId || office.Value.OfficeCoordinator == employeeId
                                 select office.Key).FirstOrDefault();
            if (officeCode == null || officeCode == "")
            {
                //  Lookup based on the office the employee is in
                UserProfile uProfile = GetUserProfile(employeeId);

                if (uProfile == null)
                {
                    return null;
                }

                officeCode = uProfile.Office;
            }

            //  Determine the current day                   
            if (db.HashExists(officeCode, "Today") == false)
            {
                db.HashSet(officeCode, "Today", DateTime.Now.DayOfWeek.ToString());
            }
            DayOfWeek Today = (DayOfWeek)Enum.Parse(typeof(DayOfWeek), db.HashGet(officeCode, "Today"));

            List<EmployeeTimesheetStatus> statuses = new List<EmployeeTimesheetStatus>();
            foreach (string emp in OfficeUsers[officeCode]) 
            {
                UserProfile profile = GetUserProfile(emp);
                EmployeeTimesheetStatus status = new EmployeeTimesheetStatus()
                {
                    EmployeeId = emp,
                    FirstName = profile.FirstName,
                    LastName = profile.LastName,
                    Office = profile.Office,
                    DailySaveRequired = profile.DailySaveRequired,
                };

                string totals = db.StringGet("Totals:" + emp); 
                if (totals == null)
                {
                    totals = "{ \"Total\": 0 }";
                }
                Dictionary<string, Decimal?> hours = JsonConvert.DeserializeObject<Dictionary<string, Decimal?>>(totals);
                hours = GetHoursForDay(hours, Today);                   

                status.Monday = hours["Monday"];
                status.Tuesday = hours["Tuesday"];
                status.Wednesday = hours["Wednesday"];
                status.Thursday = hours["Thursday"];
                status.Friday = hours["Friday"];
                status.TotalHours = hours["Total"];// GetHoursForDay(hours, "Total", Today);

                statuses.Add(status);
            }


            return new OfficeProfile()
            {
                Today = Today,
                BranchManager = GpSqlService.GetBranchManager(officeCode),
                OfficeCoordinator = GpSqlService.GetOfficeCoordinator(officeCode),
                CurrentEmployeeStatus = statuses                
            };
        }
               
    }
}