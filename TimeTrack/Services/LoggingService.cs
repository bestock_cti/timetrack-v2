﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Models;

namespace TimeTrack.Services
{
    public class LoggingService
    {
        private struct LogMessage
        {
            public string Source { get; set; }
            public string Type {get; set; }
            public string EventName { get; set; }
            public string Message { get; set; }
            public string DynamicData { get; set; }
            public string TimestampUTC { get; set; }
            //  ---[ IFF Source=="Web" ]---
            public string UserId { get; set; }
            public bool FromBrowser { get; set; }
            //  ---[ IFF Type=="Error" ]---
            public bool FatalError { get; set; }
        }

        private static int IDCounter { get; set; }

        private static void WriteLogMessage(LogMessage msg)
        {
            //  Do something with these log messages!!!

        }

        public static void WebInfo(string userId, bool fromBrowser, string eventName, string message, dynamic data)
        {
            WriteLogMessage(new LogMessage()
            {
                Source = "Web",
                Type = "Info",
                EventName = eventName,
                Message = message,
                DynamicData = JsonConvert.SerializeObject(data),
                TimestampUTC = DateTime.UtcNow.ToString(),
                UserId = userId,
                FromBrowser = fromBrowser                
            });
        }
        public static void WebError(string userId, bool fromBrowser, string eventName, string message, dynamic data)
        {
            WriteLogMessage(new LogMessage()
            {
                Source = "Web",
                Type = "Error",
                FatalError = false,
                EventName = eventName,
                Message = message,
                DynamicData = JsonConvert.SerializeObject(data),
                TimestampUTC = DateTime.UtcNow.ToString(),
                UserId = userId,
                FromBrowser = fromBrowser
            });
        }
        public static void WebReport(string userId, string reportType, string message, dynamic data)
        {
            WriteLogMessage(new LogMessage()
            {
                Source = "Web",
                Type = "Report",
                EventName = reportType,
                Message = message,
                DynamicData = JsonConvert.SerializeObject(data),
                TimestampUTC = DateTime.UtcNow.ToString(),
                UserId = userId,
                FromBrowser = true
            });
        }

        public static void Info(string eventName, string message, dynamic data)
        {
            WriteLogMessage(new LogMessage()
            {
                Source = "Internal",
                Type = "Info",
                EventName = eventName,
                Message = message,
                DynamicData = JsonConvert.SerializeObject(data),
                TimestampUTC = DateTime.UtcNow.ToString()
            });
        }
        public static void ConfigurationError(string eventName, string message, dynamic data)
        {
            WriteLogMessage(new LogMessage()
            {
                Source = "Internal",
                Type = "ConfigurationError",
                FatalError = false,
                EventName = eventName,
                Message = message,
                DynamicData = JsonConvert.SerializeObject(data),
                TimestampUTC = DateTime.UtcNow.ToString()
            });
        }
        public static void FatalError(string eventName, string message, dynamic data)
        {
            WriteLogMessage(new LogMessage()
            {
                Source = "Internal",
                Type = "Error",
                FatalError = true,
                EventName = eventName,
                Message = message,
                DynamicData = JsonConvert.SerializeObject(data),
                TimestampUTC = DateTime.UtcNow.ToString()
            });
        }
        
        // ---------- Redis Logging -----------------
        
        public static void RecordUserLogout(string username, DateTime loginTime, long duration, bool clean)
        {
            LoginRecord record = new LoginRecord()
            {
                SessionDuration = duration,
                CleanlyClosed = clean,
                Timestamp = loginTime
            };

            IDatabase db = RedisService.GetLoggingDB();
            db.SetAdd("Logins:" + username, JsonConvert.SerializeObject(record));
            string statsKey = "Stats:" + username;
            long durationAverage = (long)db.HashGet(statsKey, "DurationAverage");
            int totalLoginEvents = (int)db.HashGet(statsKey, "TotalLoginEvents");
            if (totalLoginEvents == null)
            {
                totalLoginEvents = 0;
            }                        
            db.HashSet(statsKey, "TotalLoginEvents", (totalLoginEvents + 1));
            if (durationAverage == null)
            {
                durationAverage = duration;
            }
            else
            {
                durationAverage = ((durationAverage * totalLoginEvents) + duration) / (totalLoginEvents + 1);
            }
            db.HashSet(statsKey, "DurationAverage", durationAverage);
            if (clean)
            {
                db.HashIncrement(statsKey, "CleanLogouts");
            }
            else
            {
                db.HashIncrement(statsKey, "DirtyLogouts");
            }
        }
        public static void RecordUserBackup(string username, DateTime when, List<TimeEntry> entries)
        {
            //  STUB: void RecordUserBackup(string username, DateTime when, Timesheet timesheet)
        }
        public static void CleanUpUserRecords(string username)
        {
            //  Totally remove all stored information for this user
            IDatabase db = RedisService.GetLoggingDB();
            db.KeyDelete("Stats:" + username);
            db.KeyDelete("Logins:" + username);
            //db.KeyDelete("Backups:" + username);
        }

    }
}