﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using Newtonsoft.Json;
using TimeTrack.Models;

namespace TimeTrack.Services.Proxy
{
    public partial class TimeSheetTrx
    {

        public TimeSheetTrx(string employeeId, DayOfWeek day, int seqNumber, TimeEntry entry, DateTime weekEnding, bool jobCost)
        {
            decimal payRate = GpSqlService.GetPayRateForEmployee(employeeId, TimeType.FromSubType(entry.SubType).PayCode);
                                            
            decimal? hours = 0;
            switch (day)
            {
                case DayOfWeek.Saturday:
                    hours = entry.Saturday;
                    this.DYOFWEEK = "SATURDAY";
                    this.Saturday = (int)Math.Floor(hours.Value * 100);  
                    this.WS_Transaction_Date = weekEnding.AddDays(-6);
                    break;
                case DayOfWeek.Sunday:
                    hours = entry.Sunday;
                    this.DYOFWEEK = "SUNDAY";
                    this.Sunday = (int)Math.Floor(hours.Value * 100);
                    this.WS_Transaction_Date = weekEnding.AddDays(-5);
                    break;
                case DayOfWeek.Monday:
                    hours = entry.Monday;
                    this.DYOFWEEK = "MONDAY";
                    this.Monday = (int)Math.Floor(hours.Value * 100);
                    this.WS_Transaction_Date = weekEnding.AddDays(-4);
                    break;
                case DayOfWeek.Tuesday:
                    hours = entry.Tuesday;   
                    this.DYOFWEEK = "TUESDAY";
                    this.Tuesday = (int)Math.Floor(hours.Value * 100);
                    this.WS_Transaction_Date = weekEnding.AddDays(-3);
                    break;
                case DayOfWeek.Wednesday:
                    hours = entry.Wednesday; 
                    this.DYOFWEEK = "WEDNESDAY";
                    this.Wednesday = (int)Math.Floor(hours.Value * 100); 
                    this.WS_Transaction_Date = weekEnding.AddDays(-2);
                    break;
                case DayOfWeek.Thursday:
                    hours = entry.Thursday;  
                    this.DYOFWEEK = "THURSDAY";
                    this.Thursday = (int)Math.Floor(hours.Value * 100);
                    this.WS_Transaction_Date = weekEnding.AddDays(-1);
                    break;
                case DayOfWeek.Friday:
                    hours = entry.Friday;    
                    this.DYOFWEEK = "FRIDAY";
                    this.Friday = (int)Math.Floor(hours.Value * 100);  
                    this.WS_Transaction_Date = weekEnding.AddDays(0);
                    break;
            }


            decimal totalPayment = payRate * hours.Value;

            this.PAYRTAMT = this.Cost_Per_Unit = payRate;
            this.TRXHRUNT = hours.Value;


            this.Cost_Code_Act_Cost_TTD = totalPayment;

            this.TRXBEGDT = new DateTime(weekEnding.AddDays(-6).Ticks);
            this.TRXENDDT = new DateTime(weekEnding.Ticks);
            this.MODIFDT = DateTime.Today;
            this.Modified_Time = DateTime.Parse("1/1/1990 " + DateTime.Now.ToShortTimeString());    // creates a DateTime instance that ignores date

            this.BACHNUMB = GpSqlService.BuildBatchId(employeeId, weekEnding, day);
            this.EMPLOYID = employeeId.ToUpper();            // ToUpper() used just in case a lower case version slips through somewhere
            this.SEQNUMBR = this.User_Def_Integer_1 = seqNumber;

            TimeType timeType = TimeType.FromSubType(entry.SubType);
                        
            this.UPRTRXCD = timeType.PayCode;
            this.Cost_Code_Number_2 = timeType.CostCode;
            this.User_Define_2 = entry.Customer;

            if (jobCost && timeType.Type == "Job")
            {
                this.WS_Transaction_Type = "JOB_COST";
                this.Cost_Element = 1;
                this.Profit_Type_Number = 2;
            }
            else
            {
                this.WS_Transaction_Type = "UNBILLED";
                this.Cost_Element = 0;
                this.Profit_Type_Number = 0;
            }

            string division = null;
            switch (entry.Type)
            {
                case "Job":                                  
                    bool isPreSales = GpSqlService.IsPreSalesJob(entry.Id); 
                    if (isPreSales)
                    {
                        this.WS_Transaction_Type = "JOB_COST";
                        this.Profit_Type_Number = 2;
                        this.Cost_Element = 1;
                        this.WS_Job_Number = GpSqlService.BuildPreSalesIdForQuarter(entry.Id);
                        this.TRXDSCRN = entry.Id;
                    }
                    else
                    {
                        this.WS_Job_Number = entry.Id;                         
                    }
                    division = GpSqlService.DivisionForJob(entry.Id);
                    break;
                case "ServiceTicket":
                    this.TRXDSCRN = this.USERDEF1 = entry.Id;
                    division = GpSqlService.DivisionForService(entry.Id);
                    break;
                case "Rental":
                    this.TRXDSCRN = this.USERDEF1 = entry.Id;
                    break;
                case "Shop":
                    break;
                case "Holiday":
                    break;
            }

            if (division != null)
            {
                this.Cost_Code_Number_1 = division;
            }
            else
            {
                this.Cost_Code_Number_1 = "";
            }

            this.BCHSOURC = "PR_TRXENT";
            this.Document_Source = "PR";
            this.WS_Transaction_Status = 1;
            this.WS_Cost_Type = 1;
            this.MDFUSRID = "eTimePoolUser";   

            this.USRDAT01 =
                this.USRDAT02 =
                    this.user_Defined_Date_3Field =
                        this.user_Defined_Date_4Field =
                            this.Time_In =
                                this.Time_In_Date =
                                    this.Time_Out =
                                        this.Time_Out_Date = DateTime.Parse("1/1/1900");

            this.ADRSCODE =
                this.Appointment =
                    this.ASSETID = "";

            //  comptrtp?

            //  TRX_QTY_PR: total hours
            //  TRX_Origin: ""

        }

        public TimeSheetTrx(SqlDataReader dr)
        {
            LoadFromDataReader(dr);
        }

        public TimeSheetTrx(SqlDataReader dr, bool history)
        {
            LoadHistoryFromDataReader(dr);
        }

        public void LoadHistoryFromDataReader(SqlDataReader row)
        {
            this.COMPTRNM = (int)row["COMPTRNM"];
            this.BACHNUMB = Convert.ToString(row["BACHNUMB"]).Trim();
            this.BCHSOURC = Convert.ToString(row["BCHSOURC"]).Trim();
            this.Document_Source = Convert.ToString(row["Document_Source"]).Trim();
            this.EMPLOYID = Convert.ToString(row["EMPLOYID"]).Trim();
            this.WS_Transaction_Type = Convert.ToString(row["WS_Transaction_Type"]).Trim();
            this.WS_Transaction_Status = 1;// (short)row["WS_Transaction_Status"];      //========================

            this.WS_Cost_Type = (short)row["WS_Cost_Type"];
            this.Payroll_Pay_Type = (short)row["Payroll_Pay_Type"];
            this.WS_Job_Number = Convert.ToString(row["WS_Job_Number"]).Trim();
            this.Project_Number = Convert.ToString(row["Project_Number"]).Trim();
            this.Appointment = Convert.ToString(row["Appointment"]).Trim();
            this.Cost_Code_Number_1 = Convert.ToString(row["Cost_Code_Number_1"]).Trim();
            this.Cost_Code_Number_2 = Convert.ToString(row["Cost_Code_Number_2"]).Trim();
            this.Cost_Code_Number_3 = Convert.ToString(row["Cost_Code_Number_3"]).Trim();
            this.Cost_Code_Number_4 = Convert.ToString(row["Cost_Code_Number_4"]).Trim();
            this.WS_Cost_Code = (int)row["WS_Cost_Code"];
            this.Cost_Element = (short)row["Cost_Element"];
            this.LNSEQNBR = (decimal)row["LNSEQNBR"];
            this.Profit_Type_Number = (short)row["Profit_Type_Number"];
            this.Profit_Amount = (decimal)row["Profit_Amount"];
            this.COMPTRTP = (short)row["COMPTRTP"];
            this.UPRTRXCD = Convert.ToString(row["UPRTRXCD"]).Trim();
            this.TRXHRUNT = (int)row["TRXHRUNT"];
            this.Billing_Amount = (decimal)row["Billing_Amount"];
            this.PAYRTAMT = (decimal)row["PAYRTAMT"];
            this.VARDBAMT = (decimal)row["VARDBAMT"];
            this.Transaction_Pay_Period = (short)row["Transaction_Pay_Period"];
            this.Cost_Code_Act_Cost_TTD = (decimal)row["Cost_Code_Act_Cost_TTD"];
            this.TRXBEGDT = (DateTime)row["TRXBEGDT"];
            this.TRXENDDT = (DateTime)row["TRXENDDT"];
            this.WS_Transaction_Date = (DateTime)row["WS_Transaction_Date"];
            this.TRXDSCRN = Convert.ToString(row["TRXDSCRN"]).Trim();
            this.Unit_Decimal_Places = (short)row["Unit_Decimal_Places"];
            this.Cost_Decimal_Places = (short)row["Cost_Decimal_Places"];
            this.TRX_QTY_PR = (int)row["TRX_QTY_PR"];
            this.TRXQTY = (decimal)row["TRXQTY"];
            this.Cost_Per_Unit = (decimal)row["Cost_Per_Unit"];
            this.Ovrhd_1_Unit_Dec_Places = (short)row["Ovrhd_1_Unit_Dec_Places"];
            this.Overhead_1_Cst_Dec_Plcs = (short)row["Overhead_1_Cst_Dec_Plcs"];
            this.Overhead_Units_1 = (decimal)row["Overhead_Units_1"];
            this.Overhead_1_Cst_Per_Unit = (decimal)row["Overhead_1_Cst_Per_Unit"];
            this.Overhead_1_Percent = (byte)row["Overhead_1_Percent"];       
            this.Overhead_1_Pct_Rate = (decimal)row["Overhead_1_Pct_Rate"];
            this.OH_Amount = (decimal)row["OH_Amount"];
            this.ACTINDX = (int)row["ACTINDX"];
            this.UNIONCD = Convert.ToString(row["UNIONCD"]).Trim();
            this.RATECLSS = Convert.ToString(row["RATECLSS"]).Trim();
            this.DAYSWRDK = (int)row["DAYSWRDK"];
            this.WKSWRKD = (int)row["WKSWRKD"];
            this.DEPRTMNT = Convert.ToString(row["DEPRTMNT"]).Trim();
            this.JOBTITLE = Convert.ToString(row["JOBTITLE"]).Trim();
            this.STATECD = Convert.ToString(row["STATECD"]).Trim();
            this.LOCALTAX = Convert.ToString(row["LOCALTAX"]).Trim();
            this.SUTASTAT = Convert.ToString(row["SUTASTAT"]).Trim();
            this.WRKRCOMP = Convert.ToString(row["WRKRCOMP"]).Trim();
            this.FEDCLSSCD = Convert.ToString(row["FEDCLSSCD"]).Trim();
            this.Production_Actual_Qty = (decimal)row["Production_Actual_Qty"];
            this.Est_Pct_Complete_to_Date = (short)row["Est_Pct_Complete_to_Date"];
            this.Overhead_Group_Code = Convert.ToString(row["Overhead_Group_Code"]).Trim();
            this.Labor_Fixed_OH = (decimal)row["Labor_Fixed_OH"];
            this.Labor_OH_Percentage = (int)row["Labor_OH_Percentage"];
            this.Certified_Payroll = (byte)row["Certified_Payroll"];
            this.Monday = (int)row["Monday"];
            this.Tuesday = (int)row["Tuesday"];
            this.Wednesday = (int)row["Wednesday"];
            this.Thursday = (int)row["Thursday"];
            this.Friday = (int)row["Friday"];
            this.Saturday = (int)row["Saturday"];
            this.Sunday = (int)row["Sunday"];
            this.PSTGSTUS = (short)row["PSTGSTUS"];
            this.USERID = Convert.ToString(row["USERID"]).Trim();
            this.WS_Module = Convert.ToString(row["WS_Module"]).Trim();
            this.Equipment_ID = Convert.ToString(row["Equipment_ID"]).Trim();
            this.Task_Code = Convert.ToString(row["Task_Code"]).Trim();
            this.DYOFWEEK = Convert.ToString(row["DYOFWEEK"]).Trim();
            this.ADRSCODE = Convert.ToString(row["ADRSCODE"]).Trim();
            this.CUSTNMBR = Convert.ToString(row["CUSTNMBR"]).Trim();
            this.ASSETID = Convert.ToString(row["ASSETID"]).Trim();
            this.ASSETIDSUF = (short)row["ASSETIDSUF"];
            this.ASSETINDEX = (int)row["ASSETINDEX"];
            this.ITEMNMBR = Convert.ToString(row["ITEMNMBR"]).Trim();
            this.ITEMDESC = Convert.ToString(row["ITEMDESC"]).Trim();
            this.UOFM = Convert.ToString(row["UOFM"]).Trim();
            this.WS_Extended_Cost = (decimal)row["WS_Extended_Cost"];
            this.VENDORID = Convert.ToString(row["VENDORID"]).Trim();
            this.LOCNCODE = Convert.ToString(row["LOCNCODE"]).Trim();
            this.SHFTCODE = Convert.ToString(row["SHFTCODE"]).Trim();
            this.SHFTPREM = (decimal)row["SHFTPREM"];
            this.TRX_Verified = (byte)row["TRX_Verified"];
            this.TRX_Origin = Convert.ToString(row["TRX_Origin"]).Trim();
            this.Time_In = (DateTime)row["Time_In"];
            this.Time_In_Date = (DateTime)row["Time_In_Date"];
            this.Time_Out = (DateTime)row["Time_Out"];
            this.Time_Out_Date = (DateTime)row["Time_Out_Date"];
            this.WO_Task_ID = Convert.ToString(row["WO_Task_ID"]).Trim();
            this.SEQNUMBR = (int)row["SEQNUMBR"];
            this.EQSEQUIPID = Convert.ToString(row["EQSEQUIPID"]).Trim();
            this.EQS_Cost_Code = Convert.ToString(row["EQS_Cost_Code"]).Trim();
            this.Time_Zone = Convert.ToString(row["Time_Zone"]).Trim();
            this.SV_Language_ID = (short)row["SV_Language_ID"];
            this.Wennsoft_Affiliate = Convert.ToString(row["Wennsoft_Affiliate"]).Trim();
            this.Wennsoft_Branch = Convert.ToString(row["Wennsoft_Branch"]).Trim();
            this.Wennsoft_Region = Convert.ToString(row["Wennsoft_Region"]).Trim();
            // => GRPCOMPTRNM (int:4)
            this.MODIFDT = (DateTime)row["MODIFDT"];
            this.Modified_Time = (DateTime)row["Modified_Time"];
            this.MDFUSRID = Convert.ToString(row["MDFUSRID"]).Trim();
            this.User_Define_1 = Convert.ToString(row["User_Define_1"]).Trim();
            this.User_Define_2 = Convert.ToString(row["User_Define_2"]).Trim();
            this.USERDEF1 = Convert.ToString(row["USERDEF1"]).Trim();
            this.USERDEF2 = Convert.ToString(row["USERDEF2"]).Trim();
            this.User_Def_Integer_1 = (int)row["User_Def_Integer_1"];
            this.User_Def_Integer_2 = (int)row["User_Def_Integer_2"];
            this.User_Defined_Integer_3 = (int)row["User_Defined_Integer_3"];
            this.User_Defined_Integer_4 = (int)row["User_Defined_Integer_4"];
            this.User_Defined_Dollar_1 = (decimal)row["User_Defined_Dollar_1"];
            this.User_Defined_Dollar_2 = (decimal)row["User_Defined_Dollar_2"];
            this.User_Defined_Dollar_3 = (decimal)row["User_Defined_Dollar_3"];
            this.User_Defined_Dollar_4 = (decimal)row["User_Defined_Dollar_4"];
            this.USRDAT01 = (DateTime)row["USRDAT01"];
            this.USRDAT02 = (DateTime)row["USRDAT02"];
            this.User_Defined_Date_3 = (DateTime)row["User_Defined_Date_3"];
            this.User_Defined_Date_4 = (DateTime)row["User_Defined_Date_4"];
            this.User_Defined_CB_1 = (byte)row["User_Defined_CB_1"];
            this.User_Defined_CB_2 = (byte)row["User_Defined_CB_2"];
            this.User_Defined_CB_3 = (byte)row["User_Defined_CB_3"];
            this.User_Defined_CB_4 = (byte)row["User_Defined_CB_4"];
            this.WSReserved_CB1 = (byte)row["WSReserved_CB1"];
            this.WSReserved_CB2 = (byte)row["WSReserved_CB2"];
            this.WSReserved_CB3 = (byte)row["WSReserved_CB3"];
            this.WSReserved_CB4 = (byte)row["WSReserved_CB4"];
            this.WSReserved_CB5 = (byte)row["WSReserved_CB5"];
            this.WSReserved_CB6 = (byte)row["WSReserved_CB6"];
            this.WSReserved_CB7 = (byte)row["WSReserved_CB7"];
            this.WSReserved_CB8 = (byte)row["WSReserved_CB8"];
            this.WSReserved_CB9 = (byte)row["WSReserved_CB9"];
            this.WSReserved_CB10 = (byte)row["WSReserved_CB10"];
            this.BasePayRateAmount = (decimal)row["BasePayRateAmount"];
            this.EmployeeFringe = (decimal)row["EmployeeFringe"];
            this.PrevailingFringe = (decimal)row["PrevailingFringe"];
            this.CashFringe = (decimal)row["CashFringe"];
            this.EmployeeRate = (decimal)row["EmployeeRate"];
            this.EmployeeHomeRCRate = (decimal)row["EmployeeHomeRCRate"];
            this.JobTransactionRCRate = (decimal)row["JobTransactionRCRate"];
            this.WS_Posting_Status = (int)row["WS_Posting_Status"];
            // => PYADNMBR (int:4)
            // => VOIDPYADNMBR (int:4)
            // => Call_Invoice_Number (char:17)
            this.DEX_ROW_ID = (int)row["DEX_ROW_ID"];
        }

        public void LoadFromDataReader(SqlDataReader row)
        {
            this.COMPTRNM = (int)row["COMPTRNM"];
            this.BACHNUMB = Convert.ToString(row["BACHNUMB"]).Trim();
            this.BCHSOURC = Convert.ToString(row["BCHSOURC"]).Trim();
            this.Document_Source = Convert.ToString(row["Document_Source"]).Trim();
            this.EMPLOYID = Convert.ToString(row["EMPLOYID"]).Trim();
            this.WS_Transaction_Type = Convert.ToString(row["WS_Transaction_Type"]).Trim();
            this.WS_Transaction_Status = (short)row["WS_Transaction_Status"];
            this.WS_Cost_Type = (short)row["WS_Cost_Type"];
            this.Payroll_Pay_Type = (short)row["Payroll_Pay_Type"];
            this.WS_Job_Number = Convert.ToString(row["WS_Job_Number"]).Trim();
            this.Project_Number = Convert.ToString(row["Project_Number"]).Trim();
            this.Appointment = Convert.ToString(row["Appointment"]).Trim();
            this.Cost_Code_Number_1 = Convert.ToString(row["Cost_Code_Number_1"]).Trim();
            this.Cost_Code_Number_2 = Convert.ToString(row["Cost_Code_Number_2"]).Trim();
            this.Cost_Code_Number_3 = Convert.ToString(row["Cost_Code_Number_3"]).Trim();
            this.Cost_Code_Number_4 = Convert.ToString(row["Cost_Code_Number_4"]).Trim();
            this.WS_Cost_Code = (int)row["WS_Cost_Code"];
            this.Cost_Element = (short)row["Cost_Element"];
            this.LNSEQNBR = (decimal)row["LNSEQNBR"];
            this.Profit_Type_Number = (short)row["Profit_Type_Number"];
            this.Profit_Amount = (decimal)row["Profit_Amount"];
            this.COMPTRTP = (short)row["COMPTRTP"];
            this.UPRTRXCD = Convert.ToString(row["UPRTRXCD"]).Trim();
            this.TRXHRUNT = (int)row["TRXHRUNT"];
            this.Billing_Amount = (decimal)row["Billing_Amount"];
            this.PAYRTAMT = (decimal)row["PAYRTAMT"];
            this.VARDBAMT = (decimal)row["VARDBAMT"];
            this.Transaction_Pay_Period = (short)row["Transaction_Pay_Period"];
            this.Cost_Code_Act_Cost_TTD = (decimal)row["Cost_Code_Act_Cost_TTD"];
            this.TRXBEGDT = (DateTime)row["TRXBEGDT"];
            this.TRXENDDT = (DateTime)row["TRXENDDT"];
            this.WS_Transaction_Date = (DateTime)row["WS_Transaction_Date"];
            this.TRXDSCRN = Convert.ToString(row["TRXDSCRN"]).Trim();
            this.Unit_Decimal_Places = (short)row["Unit_Decimal_Places"];
            this.Cost_Decimal_Places = (short)row["Cost_Decimal_Places"];
            this.TRX_QTY_PR = (int)row["TRX_QTY_PR"];
            this.TRXQTY = (decimal)row["TRXQTY"];
            this.Cost_Per_Unit = (decimal)row["Cost_Per_Unit"];
            this.Ovrhd_1_Unit_Dec_Places = (short)row["Ovrhd_1_Unit_Dec_Places"];
            this.Overhead_1_Cst_Dec_Plcs = (short)row["Overhead_1_Cst_Dec_Plcs"];
            this.Overhead_Units_1 = (decimal)row["Overhead_Units_1"];
            this.Overhead_1_Cst_Per_Unit = (decimal)row["Overhead_1_Cst_Per_Unit"];
            this.Overhead_1_Percent = (byte)row["Overhead_1_Percent"];
            this.Overhead_1_Pct_Rate = (decimal)row["Overhead_1_Pct_Rate"];
            this.OH_Amount = (decimal)row["OH_Amount"];
            this.ACTINDX = (int)row["ACTINDX"];
            this.UNIONCD = Convert.ToString(row["UNIONCD"]).Trim();
            this.RATECLSS = Convert.ToString(row["RATECLSS"]).Trim();
            this.DAYSWRDK = (int)row["DAYSWRDK"];
            this.WKSWRKD = (int)row["WKSWRKD"];
            this.DEPRTMNT = Convert.ToString(row["DEPRTMNT"]).Trim();
            this.JOBTITLE = Convert.ToString(row["JOBTITLE"]).Trim();
            this.STATECD = Convert.ToString(row["STATECD"]).Trim();
            this.LOCALTAX = Convert.ToString(row["LOCALTAX"]).Trim();
            this.SUTASTAT = Convert.ToString(row["SUTASTAT"]).Trim();
            this.WRKRCOMP = Convert.ToString(row["WRKRCOMP"]).Trim();
            this.FEDCLSSCD = Convert.ToString(row["FEDCLSSCD"]).Trim();
            this.Production_Actual_Qty = (decimal)row["Production_Actual_Qty"];
            this.Est_Pct_Complete_to_Date = (short)row["Est_Pct_Complete_to_Date"];
            this.Overhead_Group_Code = Convert.ToString(row["Overhead_Group_Code"]).Trim();
            this.Labor_Fixed_OH = (decimal)row["Labor_Fixed_OH"];
            this.Labor_OH_Percentage = (int)row["Labor_OH_Percentage"];
            this.Certified_Payroll = (byte)row["Certified_Payroll"];
            this.Monday = (int)row["Monday"];
            this.Tuesday = (int)row["Tuesday"];
            this.Wednesday = (int)row["Wednesday"];
            this.Thursday = (int)row["Thursday"];
            this.Friday = (int)row["Friday"];
            this.Saturday = (int)row["Saturday"];
            this.Sunday = (int)row["Sunday"];
            this.PSTGSTUS = (short)row["PSTGSTUS"];
            this.USERID = Convert.ToString(row["USERID"]).Trim();
            this.WS_Module = Convert.ToString(row["WS_Module"]).Trim();
            this.Equipment_ID = Convert.ToString(row["Equipment_ID"]).Trim();
            this.Task_Code = Convert.ToString(row["Task_Code"]).Trim();
            this.DYOFWEEK = Convert.ToString(row["DYOFWEEK"]).Trim();
            this.ADRSCODE = Convert.ToString(row["ADRSCODE"]).Trim();
            this.CUSTNMBR = Convert.ToString(row["CUSTNMBR"]).Trim();
            this.ASSETID = Convert.ToString(row["ASSETID"]).Trim();
            this.ASSETIDSUF = (short)row["ASSETIDSUF"];
            this.ASSETINDEX = (int)row["ASSETINDEX"];
            this.ITEMNMBR = Convert.ToString(row["ITEMNMBR"]).Trim();
            this.ITEMDESC = Convert.ToString(row["ITEMDESC"]).Trim();
            this.UOFM = Convert.ToString(row["UOFM"]).Trim();
            this.WS_Extended_Cost = (decimal)row["WS_Extended_Cost"];
            this.VENDORID = Convert.ToString(row["VENDORID"]).Trim();
            this.LOCNCODE = Convert.ToString(row["LOCNCODE"]).Trim();
            this.SHFTCODE = Convert.ToString(row["SHFTCODE"]).Trim();
            this.SHFTPREM = (decimal)row["SHFTPREM"];
            this.TRX_Verified = (byte)row["TRX_Verified"];
            this.TRX_Origin = Convert.ToString(row["TRX_Origin"]).Trim();
            this.Time_In = (DateTime)row["Time_In"];
            this.Time_In_Date = (DateTime)row["Time_In_Date"];
            this.Time_Out = (DateTime)row["Time_Out"];
            this.Time_Out_Date = (DateTime)row["Time_Out_Date"];
            this.WO_Task_ID = Convert.ToString(row["WO_Task_ID"]).Trim();
            this.SEQNUMBR = (int)row["SEQNUMBR"];
            this.EQSEQUIPID = Convert.ToString(row["EQSEQUIPID"]).Trim();
            this.EQS_Cost_Code = Convert.ToString(row["EQS_Cost_Code"]).Trim();
            this.Time_Zone = Convert.ToString(row["Time_Zone"]).Trim();
            this.SV_Language_ID = (short)row["SV_Language_ID"];
            this.Wennsoft_Affiliate = Convert.ToString(row["Wennsoft_Affiliate"]).Trim();
            this.Wennsoft_Branch = Convert.ToString(row["Wennsoft_Branch"]).Trim();
            this.Wennsoft_Region = Convert.ToString(row["Wennsoft_Region"]).Trim();
            this.MODIFDT = (DateTime)row["MODIFDT"];
            this.Modified_Time = (DateTime)row["Modified_Time"];
            this.MDFUSRID = Convert.ToString(row["MDFUSRID"]).Trim();
            this.User_Define_1 = Convert.ToString(row["User_Define_1"]).Trim();
            this.User_Define_2 = Convert.ToString(row["User_Define_2"]).Trim();
            this.USERDEF1 = Convert.ToString(row["USERDEF1"]).Trim();
            this.USERDEF2 = Convert.ToString(row["USERDEF2"]).Trim();
            this.User_Def_Integer_1 = (int)row["User_Def_Integer_1"];
            this.User_Def_Integer_2 = (int)row["User_Def_Integer_2"];
            this.User_Defined_Integer_3 = (int)row["User_Defined_Integer_3"];
            this.User_Defined_Integer_4 = (int)row["User_Defined_Integer_4"];
            this.User_Defined_Dollar_1 = (decimal)row["User_Defined_Dollar_1"];
            this.User_Defined_Dollar_2 = (decimal)row["User_Defined_Dollar_2"];
            this.User_Defined_Dollar_3 = (decimal)row["User_Defined_Dollar_3"];
            this.User_Defined_Dollar_4 = (decimal)row["User_Defined_Dollar_4"];
            this.USRDAT01 = (DateTime)row["USRDAT01"];
            this.USRDAT02 = (DateTime)row["USRDAT02"];
            this.User_Defined_Date_3 = (DateTime)row["User_Defined_Date_3"];
            this.User_Defined_Date_4 = (DateTime)row["User_Defined_Date_4"];
            this.User_Defined_CB_1 = (byte)row["User_Defined_CB_1"];
            this.User_Defined_CB_2 = (byte)row["User_Defined_CB_2"];
            this.User_Defined_CB_3 = (byte)row["User_Defined_CB_3"];
            this.User_Defined_CB_4 = (byte)row["User_Defined_CB_4"];
            this.WSReserved_CB1 = (byte)row["WSReserved_CB1"];
            this.WSReserved_CB2 = (byte)row["WSReserved_CB2"];
            this.WSReserved_CB3 = (byte)row["WSReserved_CB3"];
            this.WSReserved_CB4 = (byte)row["WSReserved_CB4"];
            this.WSReserved_CB5 = (byte)row["WSReserved_CB5"];
            this.WSReserved_CB6 = (byte)row["WSReserved_CB6"];
            this.WSReserved_CB7 = (byte)row["WSReserved_CB7"];
            this.WSReserved_CB8 = (byte)row["WSReserved_CB8"];
            this.WSReserved_CB9 = (byte)row["WSReserved_CB9"];
            this.WSReserved_CB10 = (byte)row["WSReserved_CB10"];
            this.BasePayRateAmount = (decimal)row["BasePayRateAmount"];
            this.EmployeeFringe = (decimal)row["EmployeeFringe"];
            this.PrevailingFringe = (decimal)row["PrevailingFringe"];
            this.CashFringe = (decimal)row["CashFringe"];
            this.EmployeeRate = (decimal)row["EmployeeRate"];
            this.EmployeeHomeRCRate = (decimal)row["EmployeeHomeRCRate"];
            this.JobTransactionRCRate = (decimal)row["JobTransactionRCRate"];
            this.WS_Posting_Status = (int)row["WS_Posting_Status"];
            this.DEX_ROW_ID = (int)row["DEX_ROW_ID"];

        }

        public string getSmallJSON()
        {
            return JsonConvert.SerializeObject(new { 
                COMPTRNM = this.COMPTRNM,
                BACHNUMB = this.BACHNUMB,
                WS_Job_Number = this.WS_Job_Number,
                TRXDSCRN = this.TRXDSCRN,
                DAYSWRDK = this.DAYSWRDK,
                WKSWRKD = this.WKSWRKD,
                Monday = this.Monday,
                Tuesday = this.Tuesday,
                Wednesday = this.Wednesday,
                Thursday = this.Thursday,
                Friday = this.Friday,
                Saturday = this.Saturday,
                Sunday = this.Sunday,
                DYOFWEEK = this.DYOFWEEK,
                SEQNUMBR = this.SEQNUMBR,
                Cost_Code_Number_2 = this.Cost_Code_Number_2,       // Labor code (int)
                User_Define_2 = this.User_Define_2,  // Customer #     
                TRXHRUNT = this.TRXHRUNT,  // Hours?     
                User_Define_1 = this.User_Define_1,  // Comments    
                USERDEF2 = this.USERDEF2,  // Labor Code (string)  
            }, Formatting.Indented);
        }
        
        public override string ToString()
        {
            string s = JsonConvert.SerializeObject(this, Formatting.Indented);
            return s;
        }
    }
}