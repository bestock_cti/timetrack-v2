﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Models;

namespace TimeTrack.Services
{
    public class TimesheetService
    {

        private static List<TimeEntry> Merge(List<TimeEntry> original, List<TimeEntry> modifiedItems, DayOfWeek day) 
        {
            List<TimeEntry> o = new List<TimeEntry>();

            foreach (TimeEntry modifiedEntry in modifiedItems)
            {
                string id = modifiedEntry.GetUniqueTimeEntry().ToString();
                TimeEntry originalEntry = (from oItem in original where oItem.GetUniqueTimeEntry().ToString() == id select oItem).FirstOrDefault();
                    
                if (originalEntry == null)
                {
                    o.Add(modifiedEntry);
                    continue;
                }

                switch (day)
                {
                    case DayOfWeek.Monday:
                        originalEntry.Monday = modifiedEntry.Monday;
                        originalEntry.MondayNote = modifiedEntry.MondayNote;
                        break;
                    case DayOfWeek.Tuesday:
                        originalEntry.Tuesday = modifiedEntry.Tuesday;
                        originalEntry.TuesdayNote = modifiedEntry.TuesdayNote;
                        break;
                    case DayOfWeek.Wednesday:
                        originalEntry.Wednesday = modifiedEntry.Wednesday;
                        originalEntry.WednesdayNote = modifiedEntry.WednesdayNote;
                        break;
                    case DayOfWeek.Thursday:
                        originalEntry.Thursday = modifiedEntry.Thursday;
                        originalEntry.ThursdayNote = modifiedEntry.ThursdayNote;
                        break;
                    case DayOfWeek.Friday:
                        originalEntry.Friday = modifiedEntry.Friday;
                        originalEntry.FridayNote = modifiedEntry.FridayNote;
                        originalEntry.Saturday = modifiedEntry.Saturday;
                        originalEntry.SaturdayNote = modifiedEntry.SaturdayNote;
                        originalEntry.Sunday = modifiedEntry.Sunday;
                        originalEntry.SundayNote = modifiedEntry.SundayNote;
                        break;
                }
                o.Add(originalEntry);
            }

            foreach (TimeEntry entry in original)
            {
                string id = entry.GetUniqueTimeEntry().ToString();
                TimeEntry existingMergeEntry = (from item in o where item.GetUniqueTimeEntry().ToString() == id select item).FirstOrDefault();
                  
                if (existingMergeEntry != null)
                {
                    continue;
                }
                o.Add(entry);
            }
           
            return o;
        }

        public static Timesheet GetMergedTimesheet(string employeeId)
        {
            List<TimeEntry> frozenEntries = GpSqlService.GetLiveTimesheetEntries(employeeId, DateTime.Parse(GetCurrentWeekEnding()));
            
            IDatabase db = RedisService.GetDB();
          
            DayOfWeek today = OfficeProfileService.GetOfficeProfileByEmployee(employeeId).Today;
                        
            RedisValue rModifiedEntries = db.StringGet(employeeId);
            List<TimeEntry> modifiedEntries = new List<TimeEntry>();
            if (rModifiedEntries.HasValue) 
            {
                modifiedEntries = JsonConvert.DeserializeObject<List<TimeEntry>>(rModifiedEntries);
            }   

            return new Timesheet()
            {
                CurrentWeekDay = today.ToString(),
                WeekEndingDate = DateTime.Parse(GetCurrentWeekEnding()),
                Entries = Merge(frozenEntries, modifiedEntries, today)
            };
        }

        public static void SetModifiedEntries(string employeeId, List<TimeEntry> entries)
        {
            IDatabase db = RedisService.GetDB();

            db.StringSet(employeeId, JsonConvert.SerializeObject(entries));  
            
            LoggingService.RecordUserBackup(employeeId, DateTime.Now, entries); 
        }

        public static List<TimeEntry> GetModifiedEntries(string employeeId)
        {
            IDatabase db = RedisService.GetDB();

            string data = db.StringGet(employeeId);

            if (data == null)
            {
                return new List<TimeEntry>();
            }

            return JsonConvert.DeserializeObject<List<TimeEntry>>(data);
        }

        public static Timesheet GetFrozenTimesheet(string employeeId, string weekEnding)
        {
            GpSqlService.GetLiveTimesheetEntries(employeeId, DateTime.Parse(weekEnding));

            //  TODO: Implement the frozen timesheet lookup from history table 

            return new Timesheet()
            {
            };
        }

        public static string GetCurrentWeekEnding() 
        {
            return "12/25/2015";           //   TODO: make this the real date
        }


    }
}