﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Models;

namespace TimeTrack.Services
{
    public class ConfigurationService
    {
        //  ===> Configuration Properties START <===
        public static string ActiveDirectoryDomain = "dc01.conferencetech.com";
        public static string GpConnectionString = "server=sqlnetwork; uid=eTimePoolUser; pwd=TEST; database=" + GpSqlService.Company + "; Language=English";
        public static string CrmConnectionString = "server=sqlnetwork; uid=eTimePoolUser; pwd=TEST; database=CTICRM_MSCRM; Language=English";
        public static string MailServerHostname = "exch2013";
        

        //  ===> Configuration Properties END <===
       
        public static GlobalConfiguration GetGlobalConfiguration()
        {
            IDatabase db = RedisService.GetConfigurationDB();

            RedisValue val = db.StringGet("Global");
            if (!val.HasValue)
            {
                return new GlobalConfiguration()
                {
                    AdminUsers = new List<string>() { "BESTOCK", "PAULR", "TERRYB" },
                    ReminderEmailTime = new DateTime(1900, 1, 1, 8, 0, 0),              //  8:00 AM  1/1/1990 (date is irrelevant)
                    ReminderEmailTemplate = "You haven't updated your timesheet yet today! Please get it done ASAP."
                };
            }

            return JsonConvert.DeserializeObject<GlobalConfiguration>(val);              
        }

        public static void SetGlobalConfiguration(GlobalConfiguration configuration)
        {
            IDatabase db = RedisService.GetConfigurationDB();

            db.StringSet("Global", JsonConvert.SerializeObject(configuration));
        }

        public static OfficeConfiguration GetOfficeConfiguration(string office)
        {
            IDatabase db = RedisService.GetConfigurationDB();

            RedisValue val = db.StringGet(office);
            if (!val.HasValue)
            {
                GlobalConfiguration gc = GetGlobalConfiguration();

                return new OfficeConfiguration()
                {
                    SendReminderEmail = true,
                    ReminderEmailTime = gc.ReminderEmailTime,              
                    ReminderEmailTemplate = gc.ReminderEmailTemplate
                };
            }

            return JsonConvert.DeserializeObject<OfficeConfiguration>(val); 
        }

        public static void SetOfficeConfiguration(string office, OfficeConfiguration configuration)
        {
            IDatabase db = RedisService.GetConfigurationDB();

            db.StringSet(office, JsonConvert.SerializeObject(configuration));
        }
    }
}