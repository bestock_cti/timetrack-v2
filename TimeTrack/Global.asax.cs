﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Web.Security;
using System.Security.Principal;
using TimeTrack.Services;

namespace TimeTrack
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
          
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            ApplicationConfig.Initialize();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
            Application["CurrentUsers"] = 0;
        }

        public void Session_OnStart()
        {
            Application.Lock();
            Application["CurrentUsers"] = (int)Application["CurrentUsers"] + 1;
            Application.UnLock();

            //  - Insert the timestamp of when the session started
            Session["SessionCreatedAt"] = DateTime.Now.Ticks;
        }

        public void Session_OnEnd()
        {
            Application.Lock();
            Application["CurrentUsers"] = (int)Application["CurrentUsers"] - 1;
            Application.UnLock();

            //  - Get the UserId stored on the expiring session and set the duration key on the log
            string userId = (string)Session["UserId"];
            if (userId != null)
            {
                long duration = DateTime.Now.Ticks - (long)Session["SessionCreatedAt"];
                //LoggingService.RecordUserLogout(userId, duration);
            }
        }

    }
}