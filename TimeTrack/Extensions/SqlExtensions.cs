﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;

namespace TimeTrack.Extensions
{
    public static class SqlExtensions
    {
        public static IEnumerable<object> ExecuteObjectSet(this SqlConnection conn, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure)
        {
            var list = new List<object>();
            var columns = new List<string>(); ;

            using (var dr = conn.ExecuteReader(sql, prms, commandType))
            {
                while (dr.Read())
                {
                    var obj = new ExpandoObject() as IDictionary<string, object>;

                    if (!columns.Any())
                    {
                        if (dr.FieldCount == 0)
                            return list;

                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            columns.Add(dr.GetName(i));
                        }
                    }

                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        obj.Add(dr.GetName(i), dr[i]);
                    }

                    list.Add(obj);
                }
            }

            return list;
        }

        /// <summary>
        /// Executes a SQL statement and returns a list of primitives of given type T. The set will contain all data from the first column of each row.
        /// </summary>
        /// <typeparam name="T">The type of the primitive being returned.</typeparam>
        /// <param name="conn">The connection.</param>
        /// <param name="sql">The SQL statement.</param>
        /// <param name="prms">The parameters.</param>
        /// <param name="commandType">TimeType of the command.</param>
        /// <param name="timeout">Timeout of the command.</param>
        /// <returns></returns>
        public static IEnumerable<T> ExecuteList<T>(this SqlConnection conn, string sql, object prms = null,
                                                    CommandType commandType = CommandType.StoredProcedure, int timeout = 30) where T : struct
        {
            using (var dr = conn.ExecuteReader(sql, prms, commandType, timeout))
            {
                while (dr.Read())
                    yield return (T)Convert.ChangeType(dr.GetValue(0), typeof(T));
            }
        }

        public static void ExecuteNonQuery(this SqlTransaction trans, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure, int timeout = 30)
        {
            ExecuteNonQuery(sql, trans.Connection, trans, prms, commandType, timeout);
        }

        public static void ExecuteNonQuery(this SqlConnection conn, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure, int timeout = 30)
        {
            ExecuteNonQuery(sql, conn, null, prms, commandType, timeout);
        }

        private static void ExecuteNonQuery(string sql, SqlConnection conn, SqlTransaction trans, object prms, CommandType commandType, int timeout)
        {
            var cmd = PrepareCommand(sql, commandType, conn, prms, trans, timeout);
            cmd.ExecuteNonQuery();
        }

        public static object ExecuteScalar(this SqlConnection conn, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure)
        {
            using (var dr = conn.ExecuteReader(sql, prms, commandType))
            {
                if (!dr.HasRows)
                {
                    return null;
                }

                if (dr.Read())
                    return dr[0];

                return null;
            }
        }

        public static T ExecuteScalar<T>(this SqlConnection conn, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure)
        {
            var ret = conn.ExecuteScalar(sql, prms, commandType);
            if (ret == null)
                return default(T);
            return (T)Convert.ChangeType(ret, typeof(T));
        }

        public static SqlDataReader ExecuteReader(this SqlTransaction trans, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure, int timeout = 30)
        {
            return ExecuteReader(sql, trans.Connection, prms, trans, commandType);
        }

        public static object ExecuteScalar(this SqlTransaction trans, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure, int timeout = 30)
        {
            using (var dr = ExecuteReader(sql, trans.Connection, prms, trans, commandType))
            {
                if (!dr.HasRows)
                {
                    return null;
                }

                if (dr.Read())
                    return dr[0];

                return null;
            }
        }

        public static T ExecuteScalar<T>(this SqlTransaction trans, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure)
        {
            var ret = trans.ExecuteScalar(sql, prms, commandType);
            if (ret == null) return default(T);
            return (T)Convert.ChangeType(ret, typeof(T));
        }

        public static SqlDataReader ExecuteReader(this SqlConnection conn, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure, int timeout = 30)
        {
            return ExecuteReader(sql, conn, prms, null, commandType, timeout);
        }

        private static SqlDataReader ExecuteReader(string sql, SqlConnection conn, object prms, SqlTransaction trans, CommandType commandType, int timeout = 30)
        {
            var cmd = PrepareCommand(sql, commandType, conn, prms, trans, timeout);
            return cmd.ExecuteReader();
        }

        public static DataTable ExecuteDataTable(this SqlConnection conn, string sql, object prms = null, CommandType commandType = CommandType.StoredProcedure, int timeout = 30)
        {
            var cmd = PrepareCommand(sql, commandType, conn, prms, null, timeout);

            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();

            da.Fill(ds);

            if (ds.Tables.Count > 0)
            {
                return ds.Tables[0];
                //assume there is only one table
            }

            return null;
        }

        private static SqlCommand PrepareCommand(string sql, CommandType commandType, SqlConnection conn, object prms, SqlTransaction trans, int timeout)
        {
            var cmd = new SqlCommand(sql.Trim(), conn, trans)
            {
                CommandType = commandType,
                CommandTimeout = timeout
            };

            var values = prms as IEnumerable<SqlParameter>;
            if (values != null)
                cmd.Parameters.AddRange(values.ToArray());
            else
                cmd.Parameters.AddRange(GetParamsFromObjectProperties(prms));

            return cmd;
        }

        private static SqlParameter GetParam(string paramName, object value)
        {
            return new SqlParameter(paramName, value ?? DBNull.Value);
        }

        private static SqlParameter[] GetParamsFromObjectProperties(object prms)
        {
            var list = new List<SqlParameter>();
            if (prms != null)
            {
                list.AddRange(prms.GetType().GetProperties().Select(p => GetParam("@" + p.Name, p.GetValue(prms, null))));
            }
            return list.ToArray();
        }
    }
}
