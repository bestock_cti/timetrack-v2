﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TimeTrack.Services;

namespace TimeTrack
{
    public class ApplicationConfig
    {
        public static void Initialize()
        {
            //  Load the redis service since all the configuration information is stored there
            RedisService.Connect();
                      
            //  Initialize Data Services
            ActiveDirectoryService.Initalize(ConfigurationService.ActiveDirectoryDomain);
            CrmSqlService.Connect(ConfigurationService.CrmConnectionString);
            GpSqlService.Connect(ConfigurationService.GpConnectionString);
                        
            //  TODO: Initialize the reminder service


            OrganizationService.RefreshUserPictureCache();
            OrganizationService.CheckCachedProfiles();

        }
    }
}