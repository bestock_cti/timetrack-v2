﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrack.Testing
{
    public enum LaborCode
    {
        Rental,
        FieldService,
        ShopTime,
        PersonalVacation,
        Holiday,
        Drafting,
        DriveTime,
        Engineering,   //7
        InstallInHouse,
        InstallOnSite,
        InHouseCtrlPrgm,
        OnSiteCtrlPrgm,
        AudioProgramming,
        TouchPanelDesign,
        ProjMgmt     //14
    }

    public enum Division
    {
        STL = 1,
        KC = 2,
        MEM = 3,
        OMA = 4,
        PEO = 5,
        WIC = 6,
        LIT = 7,
        SD = 8,
        CR = 9,
        DES = 10,
        QC = 11,
        MIL = 12,
        SJ = 13,
        CHI = 14,
        IVD = 88,
        SFB = 16,
        PHX = 17,
        HOU = 18        
    }

    public enum TimeType
    {
        Job = 11,
        ServiceTicket = 22,
        ShopType = 33,
        HolidayPersonal = 44,
        Rental = 55
    }

    public enum TransactionType
    {
        JOB_COST,
        UNBILLED
    }

    public class OldTransaction
    {     
        public string ServiceTicketNumber { get; set; }   
        public string JobNumber { get; set; }           
        public string OriginalJobNumber { get; set; }   
        public string CustomerNumber { get; set; }  
        public string RentalWriteIn { get; set; } 
        public string Comments { get; set; }     
        public LaborCode LaborCode { get; set; } 
        public bool IsWriteIn { get; set; }
        public bool IsPreSale { get; set; }
        public Division? Division { get; set; }
        

        public decimal MondayHours { get; set; }
        public decimal TuesdayHours { get; set; }
        public decimal WednesdayHours { get; set; }
        public decimal ThursdayHours { get; set; }
        public decimal FridayHours { get; set; }
        public decimal SaturdayHours { get; set; }
        public decimal SundayHours { get; set; }

        public TimeType TimeType { get; set; }

        public TransactionType TransactionType
        {
            get
            {
                if (TimeType == TimeType.Job && !string.IsNullOrWhiteSpace(JobNumber) && !IsWriteIn)
                {
                    return TransactionType.JOB_COST;
                }
                return TransactionType.UNBILLED;
            }
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public OldTransaction()
        {
            TimeSheetTrxIds = new List<int>();
        }

        public List<int> TimeSheetTrxIds { get; private set; }

        public int? FindKeyForDay(DayOfWeek day)
        {
            return null;
        }
    }
}