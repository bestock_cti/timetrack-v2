﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TimeTrack.Models;
using TimeTrack.Services;

namespace TimeTrack.Controllers
{
    public class TimesheetController : ApiController
    {
        public IHttpActionResult Get(string employeeId) 
        {
            string AuthenticatedEmployeeId = (string)HttpContext.Current.Session["EmployeeId"];
            if (AuthenticatedEmployeeId == null)
            {
                return Unauthorized();
            }

            return Ok(TimesheetService.GetMergedTimesheet(employeeId));
        }

        //  TODO : Finish implementing Frozen Timesheet lookup!
        public IHttpActionResult Get(string employeeId, string weekEnding)
        {
            string AuthenticatedEmployeeId = (string)HttpContext.Current.Session["EmployeeId"];
            if (AuthenticatedEmployeeId == null)
            {
                return Unauthorized();
            }

            if (weekEnding == "")
            {
                //set to the current week ending date
                weekEnding = TimesheetService.GetCurrentWeekEnding();
            }

            return Ok(TimesheetService.GetFrozenTimesheet(employeeId, weekEnding));  //    GetFrozenTimesheet(employeeId, weekEndingDate)
        }


        public IHttpActionResult Post(List<TimeEntry> ModifiedEntries)
        {
            string AuthenticatedEmployeeId = (string)HttpContext.Current.Session["EmployeeId"];
            if (AuthenticatedEmployeeId == null)
            {
                return Unauthorized();
            }

            TimesheetService.SetModifiedEntries(AuthenticatedEmployeeId, ModifiedEntries);

            return Ok(TimesheetService.GetMergedTimesheet(AuthenticatedEmployeeId));
        }
    }
}