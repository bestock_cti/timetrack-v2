﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TimeTrack.Models;
using TimeTrack.Services;
using TimeTrack.Extensions;

namespace TimeTrack.Controllers
{
    public class LoginController : ApiController
    {

        public bool Get()
        {
            string AuthenticatedUserId = (string)HttpContext.Current.Session["UserId"];
            if (AuthenticatedUserId == null)
            {
                //  No UserId stored in the session so it must be from an un-authenticated user
                return false; 
            }
            return true;
        }
        
        public IHttpActionResult Post([FromBody]LoginCredentials creds)
        {
            bool validCredentials = ActiveDirectoryService.ValidateCredentials(creds.Username, creds.Password);

            if (validCredentials)
            {
                //  The user is valid, store profile details on the session
                HttpContext.Current.Session["UserId"] = creds.Username;                  
                HttpContext.Current.Session["EmployeeId"] = creds.Username.NormalizeEmployeeId();

                if (creds.RememberMe)
                {
                    return Ok(new
                    {
                        EmployeeId = HttpContext.Current.Session["EmployeeId"],
                        Secret = RedisInterface.StickyCredentialsDB.GenerateSecret(creds.Username)
                    });
                }

                return Ok(new
                {
                    EmployeeId = HttpContext.Current.Session["EmployeeId"]
                });
            }
            return Unauthorized();
        }

        public IHttpActionResult Post([FromBody]StickyCredentials creds, bool isSticky)
        {
            if (RedisInterface.StickyCredentialsDB.TestSecret(creds.Username.NormalizeEmployeeId(), creds.Secret))
            {    
                HttpContext.Current.Session["UserId"] = creds.Username;
                HttpContext.Current.Session["EmployeeId"] = creds.Username.NormalizeEmployeeId();

                return Ok(new
                {
                    EmployeeId = HttpContext.Current.Session["EmployeeId"]
                });
            }
            return Unauthorized();
        }

        public IHttpActionResult Delete(string secret)
        {
            if (secret != null)
            {
                RedisInterface.StickyCredentialsDB.BurnSecret(secret);
            }
            HttpContext.Current.Session.Abandon();
            return Ok();
        }
    }
}