﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TimeTrack.Models;

namespace TimeTrack.Controllers
{
    public class StatusController : ApiController                                    
    {

        public IHttpActionResult Get()
        {
            // TODO: return a full statistics object
            var statistics = new
            {
                OfficeMigrationStatus = new Dictionary<string, dynamic>()
                {   
                    {"STL", new { Today = "Friday", Errors = new List<MigrationError>(), UsersBlocking = 0 }}
                }
            };

            return Ok();
        }


    }
}