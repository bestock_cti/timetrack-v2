﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TimeTrack.Models;
using TimeTrack.Services;
using TimeTrack.Services.Proxy;

namespace TimeTrack.Controllers
{
    public class ManagerController : ApiController
    {
        //  Returns the manager view for the current user
        public IHttpActionResult Get() 
        {
            string AuthenticatedEmployeeId = (string)HttpContext.Current.Session["EmployeeId"];
            if (AuthenticatedEmployeeId == null)
            {
                return Unauthorized();
            }

            return Ok(OfficeProfileService.GetOfficeProfileByManager(AuthenticatedEmployeeId));
        }

        //  Moves the days info into GP and increments the current day
        public IHttpActionResult Post()
        {
            string AuthenticatedEmployeeId = (string)HttpContext.Current.Session["EmployeeId"];
            if (AuthenticatedEmployeeId == null)
            {
                return Unauthorized();
            }             

            return Ok(OfficeProfileService.MigrateOffice(AuthenticatedEmployeeId));
        }
           
    }
}