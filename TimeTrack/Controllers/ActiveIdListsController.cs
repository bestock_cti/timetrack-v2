﻿using Microsoft.SharePoint.Client;
using SP = Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TimeTrack.Models;
using TimeTrack.Services;
using System.IO;

namespace TimeTrack.Controllers
{
    public class ActiveIdListsController : ApiController
    {        

        public IHttpActionResult Get()
        {
            string AuthenticatedUserId = (string)HttpContext.Current.Session["UserId"];
            if (AuthenticatedUserId == null)
            {
                //  No UserId stored in the session so it must be from an un-authenticated user
                return Unauthorized();
            }

            return Ok(CachedDataService.GetActiveIdLists());
        }

       
    }
}