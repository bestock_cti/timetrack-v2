﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TimeTrack.Services;

namespace TimeTrack.Controllers
{
    public class ConfigurationController : ApiController
    {

        public IHttpActionResult Get()
        {
            string AuthenticatedUserId = (string)HttpContext.Current.Session["UserId"];

            if (!UserProfileService.IsAdminUser(AuthenticatedUserId))
            {
                return Unauthorized();
            }

            return Ok(ConfigurationService.GetGlobalConfiguration());
        }
              
        public IHttpActionResult Post(Models.GlobalConfiguration gConfiguration)
        {
            string AuthenticatedUserId = (string)HttpContext.Current.Session["UserId"];

            if (!UserProfileService.IsAdminUser(AuthenticatedUserId))
            {
                return Unauthorized();
            }

            ConfigurationService.SetGlobalConfiguration(gConfiguration);             

            return Ok();
        }

        public IHttpActionResult Get(string office)
        {
            string AuthenticatedUserId = (string)HttpContext.Current.Session["UserId"];

            if (AuthenticatedUserId == null)
            {
                return Unauthorized();
            }

            return Ok(ConfigurationService.GetOfficeConfiguration(office));
        }

        public IHttpActionResult Post(string office, Models.OfficeConfiguration oConfiguration)
        {
            string AuthenticatedUserId = (string)HttpContext.Current.Session["UserId"];

            if (AuthenticatedUserId == null)
            {
                return Unauthorized();
            }

            ConfigurationService.SetOfficeConfiguration(office, oConfiguration);

            return Ok();       
        }
     
    }
}