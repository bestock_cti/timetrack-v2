﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using TimeTrack.Models;
using TimeTrack.Services;

namespace TimeTrack.Controllers
{
    public class UserProfileController : ApiController
    {     
        public IHttpActionResult Get(string employeeId)
        {
            string AuthenticatedEmployeeId = (string)HttpContext.Current.Session["EmployeeId"];
            if ( AuthenticatedEmployeeId == null)
            {
                return Unauthorized();
            } 
                        
            UserProfile profile = UserProfileService.GetUserProfile(employeeId);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

    }
}